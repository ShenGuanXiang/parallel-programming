/*x86平台下LU算法串行与各种Pthread并行的对比*/

#include<windows.h>
#include<iostream>
#include<cstdlib>
#include<ctime>
#include<pthread.h>
#include<semaphore.h>
#include<thread>
#include<nmmintrin.h> 
#include<immintrin.h> 
//随机数生成范围，设的比较小，防止初始化以及计算中经过多次行变换后数据溢出
#define MIN  -1
#define MAX   1
using namespace std;


const int NUM_THREADS = 8;//线程总数
int N = 200;//矩阵行数兼列数
float** m = NULL;//输入矩阵

LARGE_INTEGER head, tail, freq;//计时器(总滴答数、每秒滴答数）
int counter;//重复测试计数器
double tottime;//每次重复测试的总时间

//构造矩阵
void set() {
	m = new float* [N];
	for (int i = 0; i < N; i++)
		m[i] = new float[N];
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < i; j++)
			m[i][j] = 0;
		m[i][i] = 1.0;
		for (int j = i + 1; j < N; j++)
			m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
	}
	//每个循环随机取两行，其中一行加上另一行
	for (int t = 0; t < 15 * N; t++) {
		int i1 = rand() % N, i2 = rand() % N;
		for (int j = 0; j < N; j++)
			m[i2][j] += m[i1][j];
	}
}

//回收空间
void destroy() {
	for (int i = 0; i < N; i++)
		delete[]m[i];
	delete[]m;
}

//串行算法
void Serial() {
	for (int k = 0; k < N; k++) {
		for (int j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			for (int j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

struct Dynamic_threadParam_t {
	int k; //消去的轮次
	int t_id; // 线程 id
};

//动态线程,最大线程数限制为NUM_THREADS
void* Dynamic_threadFunc(void* param) {
	Dynamic_threadParam_t* p = (Dynamic_threadParam_t*)param;
	int k = p->k; //消去的轮次
	int t_id = p->t_id; //线程编号
	//循环划分任务
	for (int i = k + 1 + t_id; i < N; i += NUM_THREADS) {
		for (int j = k + 1; j < N; ++j)
			m[i][j] = m[i][j] - m[i][k] * m[k][j];
		m[i][k] = 0;
	}
	pthread_exit(NULL);
	return NULL;
}

void Dynamic() {
	for (int k = 0; k < N; ++k)
	{
		//主线程做除法操作
		for (int j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
		//创建工作线程，进行消去操作
		pthread_t* thread_handles = new pthread_t[NUM_THREADS];
		Dynamic_threadParam_t* param = new Dynamic_threadParam_t[NUM_THREADS];
		//分配任务
		for (int t_id = 0; t_id < NUM_THREADS; t_id++)
		{
			param[t_id].k = k;
			param[t_id].t_id = t_id;
		}
		//创建线程
		for (int t_id = 0; t_id < NUM_THREADS; t_id++)
			pthread_create(&thread_handles[t_id], NULL, Dynamic_threadFunc, (void*)&param[t_id]);
		//主线程挂起等待所有的工作线程完成此轮消去工作
		for (int t_id = 0; t_id < NUM_THREADS; t_id++)
			pthread_join(thread_handles[t_id], NULL);
		delete[]thread_handles;
		delete[]param;
	}
}

//静态线程数据结构定义
struct Static_threadParam_t {
	int t_id;//线程id
};

//信号量定义
sem_t sem_main_next_cycle;
sem_t sem_workerstart[NUM_THREADS];

//静态线程+信号量同步+三重循环（消去）纳入线程函数
void* Static_sem_threadFunc1(void* param)
{
	Static_threadParam_t* p = (Static_threadParam_t*)param;
	int t_id = p->t_id;

	for (int k = 0; k < N; ++k)
	{
		sem_wait(&sem_workerstart[t_id]);// 阻塞，等待主线程完成除法操作

		//循环划分任务
		for (int i = k + 1 + t_id; i < N; i += NUM_THREADS)
		{
			for (int j = k + 1; j < N; ++j)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}

		sem_post(&sem_main_next_cycle);// 唤醒主线程

	}
	pthread_exit(NULL);
	return NULL;
}

void Static_sem1() {
	//初始化信号量
	sem_init(&sem_main_next_cycle, 0, 0);
	for (int i = 0; i < NUM_THREADS; i++)
		sem_init(&sem_workerstart[i], 0, 0);
	//创建线程
	pthread_t thread_handles[NUM_THREADS];// 创建对应的 Handle
	Static_threadParam_t param[NUM_THREADS];// 创建对应的线程数据结构
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
	{
		param[t_id].t_id = t_id;
		pthread_create(&thread_handles[t_id], NULL, Static_sem_threadFunc1, (void*)&param[t_id]);
	}
	for (int k = 0; k < N; ++k)
	{
		//主线程做除法操作
		for (int j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
		//开始唤醒工作线程
		for (int t_id = 0; t_id < NUM_THREADS; ++t_id)
			sem_post(&sem_workerstart[t_id]);
		//主线程睡眠（等待所有的工作线程完成此轮消去任务）
		for (int t_id = 0; t_id < NUM_THREADS; ++t_id)
			sem_wait(&sem_main_next_cycle);

	}
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
		pthread_join(thread_handles[t_id], NULL);

	sem_destroy(&sem_main_next_cycle);
	for (int i = 0; i < NUM_THREADS; i++)
		sem_destroy(&sem_workerstart[i]);
}

//信号量定义
sem_t sem_leader_next_turn;
sem_t sem_Elimination_start[NUM_THREADS - 1];


//静态线程 + 信号量同步 + 除法也纳入线程函数
void *Static_sem_threadFunc2(void* param)
{
	Static_threadParam_t* p = (Static_threadParam_t*)param;
	int t_id = p->t_id;
	for (int k = 0; k < N; ++k)
	{
		// t_id 为 0 的线程做除法操作，其它工作线程先等待
		if (t_id == 0)
		{
			for (int j = k + 1; j < N; ++j)
				m[k][j] = m[k][j] / m[k][k];
			m[k][k] = 1.0;
			//唤醒其它工作线程，进行消去操作
			for (int i = 1; i < NUM_THREADS; ++i)
				sem_post(&sem_Elimination_start[i - 1]);
		}
		else {
			sem_wait(&sem_Elimination_start[t_id - 1]);
		}
		//水平循环划分任务
		for (int i = k + 1 + t_id; i < N; i += NUM_THREADS)
		{
			for (int j = k + 1; j < N; ++j)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
		//0号线程等待其它线程均结束消去操作后进入下一轮
		if (t_id == 0) {
			for (int i = 1; i < NUM_THREADS; ++i)
				sem_wait(&sem_leader_next_turn);
		}
		//其它线程结束消去操作后发出信号告诉主线程
		else
		{
			sem_post(&sem_leader_next_turn);
		}
	}
	pthread_exit(NULL);
	return NULL;
}

void Static_sem2()
{
	//初始化信号量
	sem_init(&sem_leader_next_turn, 0, 0);
	for (int i = 0; i < NUM_THREADS - 1; i++)
		sem_init(&sem_Elimination_start[i], 0, 0);
	//创建线程
	pthread_t thread_handles[NUM_THREADS];// 创建对应的 Handle
	Static_threadParam_t param[NUM_THREADS];// 创建对应的线程数据结构
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
	{
		param[t_id].t_id = t_id;
		pthread_create(&thread_handles[t_id], NULL, Static_sem_threadFunc2, (void*)&param[t_id]);
	}
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)	pthread_join(thread_handles[t_id], NULL);

	sem_destroy(&sem_leader_next_turn);
	for (int i = 0; i < NUM_THREADS - 1; i++)
		sem_destroy(&sem_Elimination_start[i]);
}

//barrier定义
pthread_barrier_t barrier_Division;
pthread_barrier_t barrier_Elimination;

//静态线程+barrier同步+三重循环（除法和消去）纳入线程函数+消去水平划分（循环划分）
void *Static_barrier_threadFunc1(void* param)
{
	Static_threadParam_t* p = (Static_threadParam_t*)param;
	int t_id = p->t_id;

	for (int k = 0; k < N; ++k)
	{
		// t_id 为 0 的线程做除法操作，其它工作线程先等待
		if (t_id == 0)
		{
			for (int j = k + 1; j < N; ++j)
				m[k][j] = m[k][j] / m[k][k];
			m[k][k] = 1.0;
		}
		pthread_barrier_wait(&barrier_Division);
		//循环划分任务
		for (int i = k + 1 + t_id; i < N; i += NUM_THREADS)
		{
			for (int j = k + 1; j < N; ++j)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
		// 所有线程一起进入下一轮
		pthread_barrier_wait(&barrier_Elimination);
	}
	pthread_exit(NULL);
	return NULL;
}

void Static_barrier1()
{
	//初始化barrier
	pthread_barrier_init(&barrier_Division, NULL, NUM_THREADS);
	pthread_barrier_init(&barrier_Elimination, NULL, NUM_THREADS);
	//创建线程
	pthread_t thread_handles[NUM_THREADS];// 创建对应的 Handle
	Static_threadParam_t param[NUM_THREADS];// 创建对应的线程数据结构
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
	{
		param[t_id].t_id = t_id;
		pthread_create(&thread_handles[t_id], NULL, Static_barrier_threadFunc1, (void*)&param[t_id]);
	}
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)	pthread_join(thread_handles[t_id], NULL);

	pthread_barrier_destroy(&barrier_Division);
	pthread_barrier_destroy(&barrier_Elimination);
}

//静态线程+barrier同步+三重循环（除法和消去）纳入线程函数+消去水平划分、除法垂直划分（均循环划分）
void *Static_barrier_threadFunc2(void* param)
{
	Static_threadParam_t* p = (Static_threadParam_t*)param;
	int t_id = p->t_id;

	for (int k = 0; k < N; ++k)
	{
		//循环划分除法任务
		for (int j = k + 1 + t_id; j < N; j += NUM_THREADS)
			m[k][j] = m[k][j] / m[k][k];
		pthread_barrier_wait(&barrier_Division);
		m[k][k] = 1.0;//每个线程在消去前都执行这一条语句(如果想要只在一个线程里执行这条语句就需要增加一个barrier)。
		//循环划分消去任务
		for (int i = k + 1 + t_id; i < N; i += NUM_THREADS)
		{
			for (int j = k + 1; j < N; ++j)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
		// 所有线程一起进入下一轮
		pthread_barrier_wait(&barrier_Elimination);
	}
	pthread_exit(NULL);
	return NULL;
}

void Static_barrier2()
{
	//初始化barrier
	pthread_barrier_init(&barrier_Division, NULL, NUM_THREADS);
	pthread_barrier_init(&barrier_Elimination, NULL, NUM_THREADS);
	//创建线程
	pthread_t thread_handles[NUM_THREADS];// 创建对应的 Handle
	Static_threadParam_t param[NUM_THREADS];// 创建对应的线程数据结构
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
	{
		param[t_id].t_id = t_id;
		pthread_create(&thread_handles[t_id], NULL, Static_barrier_threadFunc2, (void*)&param[t_id]);
	}
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)	pthread_join(thread_handles[t_id], NULL);

	pthread_barrier_destroy(&barrier_Division);
	pthread_barrier_destroy(&barrier_Elimination);
}

//静态线程+barrier同步+三重循环（除法和消去）纳入线程函数+消去水平划分(块划分)
void *Static_barrier_threadFunc3(void* param)
{
	Static_threadParam_t* p = (Static_threadParam_t*)param;
	int t_id = p->t_id;

	for (int k = 0; k < N; ++k)
	{
		// t_id 为 0 的线程做除法操作，其它工作线程先等待
		if (t_id == 0)
		{
			for (int j = k + 1; j < N; ++j)
				m[k][j] = m[k][j] / m[k][k];
			m[k][k] = 1.0;
		}
		pthread_barrier_wait(&barrier_Division);
		//块划分任务
		int d = (N - k - 1) / NUM_THREADS;//每个块包含的行数
		for (int i = k + 1 + t_id * d; i < k + 1 + (t_id + 1) * d; i++)
		{
			for (int j = k + 1; j < N; ++j)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
		//每个块的行数向下取整，最后一个线程负责剩下几行的消去
		if (t_id == NUM_THREADS - 1) {
			for (int i = k + 1 + NUM_THREADS * d; i < N; i++)
			{
				for (int j = k + 1; j < N; ++j)
					m[i][j] = m[i][j] - m[i][k] * m[k][j];
				m[i][k] = 0.0;
			}
		}
		// 所有线程一起进入下一轮
		pthread_barrier_wait(&barrier_Elimination);
	}
	pthread_exit(NULL);
	return NULL;
}

void Static_barrier3()
{
	//初始化barrier
	pthread_barrier_init(&barrier_Division, NULL, NUM_THREADS);
	pthread_barrier_init(&barrier_Elimination, NULL, NUM_THREADS);
	//创建线程
	pthread_t thread_handles[NUM_THREADS];// 创建对应的 Handle
	Static_threadParam_t param[NUM_THREADS];// 创建对应的线程数据结构
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
	{
		param[t_id].t_id = t_id;
		pthread_create(&thread_handles[t_id], NULL, Static_barrier_threadFunc3, (void*)&param[t_id]);
	}
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)	pthread_join(thread_handles[t_id], NULL);

	pthread_barrier_destroy(&barrier_Division);
	pthread_barrier_destroy(&barrier_Elimination);
}

//静态线程+barrier同步+三重循环（除法和消去）纳入线程函数+消去垂直划分(循环划分)
void *Static_barrier_threadFunc4(void* param)
{
	Static_threadParam_t* p = (Static_threadParam_t*)param;
	int t_id = p->t_id;

	for (int k = 0; k < N; ++k)
	{
		// t_id 为 0 的线程做除法操作，其它工作线程先等待
		if (t_id == 0)
		{
			for (int j = k + 1; j < N; ++j)
				m[k][j] = m[k][j] / m[k][k];
			m[k][k] = 1.0;
		}
		pthread_barrier_wait(&barrier_Division);
		//垂直循环划分任务
		for (int i = k + 1; i < N; i++)
		{
			for (int j = k + 1 + t_id; j < N; j += NUM_THREADS)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
		}
		// 所有线程一起进入下一轮
		pthread_barrier_wait(&barrier_Elimination);
		//垂直划分时，各个线程消去都结束后才能将各行首项置0
		if (t_id == 0)
		{
			for (int i = k + 1; i < N; i++) {
				m[i][k] = 0.0;
			}
		}
	}
	pthread_exit(NULL);
	return NULL;
}

void Static_barrier4()
{
	//初始化barrier
	pthread_barrier_init(&barrier_Division, NULL, NUM_THREADS);
	pthread_barrier_init(&barrier_Elimination, NULL, NUM_THREADS);
	//创建线程
	pthread_t thread_handles[NUM_THREADS];// 创建对应的 Handle
	Static_threadParam_t param[NUM_THREADS];// 创建对应的线程数据结构
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
	{
		param[t_id].t_id = t_id;
		pthread_create(&thread_handles[t_id], NULL, Static_barrier_threadFunc4, (void*)&param[t_id]);
	}
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)	pthread_join(thread_handles[t_id], NULL);

	pthread_barrier_destroy(&barrier_Division);
	pthread_barrier_destroy(&barrier_Elimination);
}

//静态线程+barrier同步+三重循环（除法和消去）纳入线程函数+消去水平划分（循环划分）+消去、除法SSE向量化
void *Static_barrier_threadFunc5(void* param)
{
	Static_threadParam_t* p = (Static_threadParam_t*)param;
	int t_id = p->t_id;

	for (int k = 0; k < N; ++k)
	{
		// t_id 为 0 的线程SSE向量化除法操作
		if (t_id == 0)
		{
			__m128 vkk = _mm_set1_ps(m[k][k]);
			int j = k + 1;
			for (; j + 4 <= N; j += 4) {
				__m128 vkj = _mm_loadu_ps(&m[k][j]);
				vkj = _mm_div_ps(vkj, vkk);
				_mm_storeu_ps(&m[k][j], vkj);
			}
			for (; j < N; j++) {
				m[k][j] = m[k][j] / m[k][k];
			}
			m[k][k] = 1.0;
		}
		pthread_barrier_wait(&barrier_Division);
		//循环划分任务
		for (int i = k + 1 + t_id; i < N; i += NUM_THREADS)
		{
			//每行的消去SSE向量化
			__m128 vik = _mm_set1_ps(m[i][k]);
			int j = k + 1;
			for (; j + 4 <= N; j += 4) {
				__m128 vkj = _mm_loadu_ps(&m[k][j]);
				__m128 vij = _mm_loadu_ps(&m[i][j]);
				vij = _mm_sub_ps(vij, _mm_mul_ps(vik, vkj));
				_mm_storeu_ps(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
		// 所有线程一起进入下一轮
		pthread_barrier_wait(&barrier_Elimination);
	}
	pthread_exit(NULL);
	return NULL;
}

void Static_barrier5()
{
	//初始化barrier
	pthread_barrier_init(&barrier_Division, NULL, NUM_THREADS);
	pthread_barrier_init(&barrier_Elimination, NULL, NUM_THREADS);
	//创建线程
	pthread_t thread_handles[NUM_THREADS];// 创建对应的 Handle
	Static_threadParam_t param[NUM_THREADS];// 创建对应的线程数据结构
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
	{
		param[t_id].t_id = t_id;
		pthread_create(&thread_handles[t_id], NULL, Static_barrier_threadFunc5, (void*)&param[t_id]);
	}
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)	pthread_join(thread_handles[t_id], NULL);

	pthread_barrier_destroy(&barrier_Division);
	pthread_barrier_destroy(&barrier_Elimination);
}

//静态线程+barrier同步+三重循环（除法和消去）纳入线程函数+消去水平划分（循环划分）+消去、除法AVX向量化
void *Static_barrier_threadFunc6(void* param)
{
	Static_threadParam_t* p = (Static_threadParam_t*)param;
	int t_id = p->t_id;

	for (int k = 0; k < N; ++k)
	{
		// AVX向量化除法操作
		if (t_id == 0)
		{
			__m256 vkk = _mm256_set1_ps(m[k][k]);
			int j = k + 1;
			for (; j + 8 <= N; j += 8) {
				__m256 vkj = _mm256_loadu_ps(&m[k][j]);
				vkj = _mm256_div_ps(vkj, vkk);
				_mm256_storeu_ps(&m[k][j], vkj);
			}
			for (; j < N; j++) {
				m[k][j] = m[k][j] / m[k][k];
			}
		}
		pthread_barrier_wait(&barrier_Division);
		//循环划分任务
		for (int i = k + 1 + t_id; i < N; i += NUM_THREADS)
		{
			//每行的消去AVX向量化
			__m256 vik = _mm256_set1_ps(m[i][k]);
			int j = k + 1;
			for (; j + 8 <= N; j += 8) {
				__m256 vkj = _mm256_loadu_ps(&m[k][j]);
				__m256 vij = _mm256_loadu_ps(&m[i][j]);
				vij = _mm256_sub_ps(vij, _mm256_mul_ps(vik, vkj));
				_mm256_storeu_ps(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
		// 所有线程一起进入下一轮
		pthread_barrier_wait(&barrier_Elimination);
	}
	pthread_exit(NULL);
	return NULL;
}

void Static_barrier6()
{
	//初始化barrier
	pthread_barrier_init(&barrier_Division, NULL, NUM_THREADS);
	pthread_barrier_init(&barrier_Elimination, NULL, NUM_THREADS);
	//创建线程
	pthread_t thread_handles[NUM_THREADS];// 创建对应的 Handle
	Static_threadParam_t param[NUM_THREADS];// 创建对应的线程数据结构
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
	{
		param[t_id].t_id = t_id;
		pthread_create(&thread_handles[t_id], NULL, Static_barrier_threadFunc6, (void*)&param[t_id]);
	}
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)	pthread_join(thread_handles[t_id], NULL);

	pthread_barrier_destroy(&barrier_Division);
	pthread_barrier_destroy(&barrier_Elimination);
}


//静态线程+barrier同步+三重循环（除法和消去）纳入线程函数+消去水平划分（循环划分）+消去、除法AVX512向量化
void *Static_barrier_threadFunc7(void* param)
{
	Static_threadParam_t* p = (Static_threadParam_t*)param;
	int t_id = p->t_id;

	for (int k = 0; k < N; ++k)
	{
		// AVX512向量化除法操作
		if (t_id == 0)
		{
			__m512 vkk = _mm512_set1_ps(m[k][k]);
			int j = k + 1;
			for (; j + 16 <= N; j += 16) {
				__m512 vkj = _mm512_loadu_ps(&m[k][j]);
				vkj = _mm512_div_ps(vkj, vkk);
				_mm512_storeu_ps(&m[k][j], vkj);
			}
			for (; j < N; j++) {
				m[k][j] = m[k][j] / m[k][k];
			}
			m[k][k] = 1.0;
		}
		pthread_barrier_wait(&barrier_Division);
		//循环划分任务
		for (int i = k + 1 + t_id; i < N; i += NUM_THREADS)
		{
			//每行的消去AVX512向量化
			__m512 vik = _mm512_set1_ps(m[i][k]);
			int j = k + 1;
			for (; j + 16 <= N; j += 16) {
				__m512 vkj = _mm512_loadu_ps(&m[k][j]);
				__m512 vij = _mm512_loadu_ps(&m[i][j]);
				vij = _mm512_sub_ps(vij, _mm512_mul_ps(vik, vkj));
				_mm512_storeu_ps(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
		// 所有线程一起进入下一轮
		pthread_barrier_wait(&barrier_Elimination);
	}
	pthread_exit(NULL);
	return NULL;
}

void Static_barrier7()
{
	//初始化barrier
	pthread_barrier_init(&barrier_Division, NULL, NUM_THREADS);
	pthread_barrier_init(&barrier_Elimination, NULL, NUM_THREADS);
	//创建线程
	pthread_t thread_handles[NUM_THREADS];// 创建对应的 Handle
	Static_threadParam_t param[NUM_THREADS];// 创建对应的线程数据结构
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
	{
		param[t_id].t_id = t_id;
		pthread_create(&thread_handles[t_id], NULL, Static_barrier_threadFunc7, (void*)&param[t_id]);
	}
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)	pthread_join(thread_handles[t_id], NULL);

	pthread_barrier_destroy(&barrier_Division);
	pthread_barrier_destroy(&barrier_Elimination);
}


//条件量
int flag_Division;
int flag_Elimination;

//静态线程+忙等待（条件量）+三重循环（除法、消去）纳入线程+消去水平划分（循环划分）
void *Static_busywaiting_threadFunc(void* param) {
	Static_threadParam_t* p = (Static_threadParam_t*)param;
	int t_id = p->t_id;

	for (int k = 0; k < N; ++k)
	{
		// t_id 为 0 的线程做除法操作，其它工作线程先等待
		if (t_id == 0)
		{
			for (int j = k + 1; j < N; ++j)
				m[k][j] = m[k][j] / m[k][k];
			m[k][k] = 1.0;
			flag_Division++;
		}
		else {
			while (flag_Division != (k + 1)) std::this_thread::yield();
		}
		//循环划分任务
		for (int i = k + 1 + t_id; i < N; i += NUM_THREADS)
		{
			for (int j = k + 1; j < N; ++j)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
		//在flag_Elimination达到线程编号对应的值时才递增
		while (flag_Elimination < (k * NUM_THREADS + t_id)) std::this_thread::yield();
		flag_Elimination++;
		// 所有线程一起进入下一轮
		while (flag_Elimination < ((k + 1) * NUM_THREADS)) std::this_thread::yield();
	}
	pthread_exit(NULL);
	return NULL;
}

void Static_busywaiting()
{
	//初始化flag
	flag_Division = flag_Elimination = 0;
	//创建线程
	pthread_t thread_handles[NUM_THREADS];// 创建对应的 Handle
	Static_threadParam_t param[NUM_THREADS];// 创建对应的线程数据结构
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
	{
		param[t_id].t_id = t_id;
		pthread_create(&thread_handles[t_id], NULL, Static_busywaiting_threadFunc, (void*)&param[t_id]);
	}
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)	pthread_join(thread_handles[t_id], NULL);
}





//void print() {
//	for (int i = 0; i < N; i++) {
//		for (int j = 0; j < N; j++)
//			cout << m[i][j] << " ";
//		cout << endl;
//	}
//	cout << endl;
//}
//
////用来验证代码正确性，测量时间时无需调用
//void Verify() {
//	set();
//	print();
//	Serial();
//	print();
//	destroy();
//	set();
//	print();
//	Dynamic();
//	print();
//	destroy();
//	set();
//	print();
//	Static_sem1();
//	print();
//	destroy();
//	set();
//	print();
//	Static_sem2();
//	print();
//	destroy();
//	set();
//	print();
//	Static_barrier1();
//	print();
//	destroy();
//	set();
//	print();
//	Static_barrier2();
//	print();
//	destroy();
//	set();
//	print();
//	Static_barrier3();
//	print();
//	destroy();
//	set();
//	print();
//	Static_barrier4();
//	print();
//	destroy();
//	set();
//	print();
//	Static_busywaiting();
//	print();
//	destroy();
//}

//主函数对以上各种高斯消去法的函数分别计时，采用重复测试方法
int main() {
	srand(static_cast <unsigned> (time(0)));
	srand((unsigned)time(NULL));
	QueryPerformanceFrequency(&freq);

	do {
		set();
		QueryPerformanceCounter(&head);
		Serial();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Serial(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set();
		QueryPerformanceCounter(&head);
		Dynamic();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Dynamic(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set();
		QueryPerformanceCounter(&head);
		Static_sem1();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Static_sem1(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set();
		QueryPerformanceCounter(&head);
		Static_sem2();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Static_sem2(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set();
		QueryPerformanceCounter(&head);
		Static_barrier1();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Static_barrier1(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set();
		QueryPerformanceCounter(&head);
		Static_barrier2();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Static_barrier2(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set();
		QueryPerformanceCounter(&head);
		Static_barrier3();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Static_barrier3(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;


	do {
		set();
		QueryPerformanceCounter(&head);
		Static_barrier4();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Static_barrier4(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set();
		QueryPerformanceCounter(&head);
		Static_barrier5();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Static_barrier5(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set();
		QueryPerformanceCounter(&head);
		Static_barrier6();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Static_barrier6(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set();
		QueryPerformanceCounter(&head);
		Static_barrier7();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Static_barrier7(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set();
		QueryPerformanceCounter(&head);
		Static_busywaiting();
		QueryPerformanceCounter(&tail);
		destroy();
		counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 10);
	cout << "Static_busywaiting(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

}