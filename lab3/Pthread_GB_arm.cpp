/*arm平台下特殊高斯消去算法串行与Pthread并行的对比*/

#include<stdio.h>
#include<time.h>
#include<iostream>
#include<cstdlib>
#include<ctime>
#include<pthread.h>
#include<semaphore.h>
#include<thread>
#include<cstring>
#include<fstream>
#include<sstream>
using namespace std;

const int NUM_THREADS = 8;//线程总数
const int N = 130;//矩阵列数
const int m = 8;//被消元行总行数
string elimination_txt = "1_130_22_8//1.txt", deleted_row_txt = "1_130_22_8//2.txt", result_txt = "1_130_22_8//3.txt";//消元子、被消元行、消元结果文件路径
const int len = (N - 1) / 8 + 1;//Mybitset中bits数组元素个数(N/8向上取整)

struct timespec sts, ets;
time_t dsec;
long long dnsec;
int counter;//重复测试计数器
long long tottime;//每次重复测试的总时间


//bit位从右（高地址）向左（低地址）开始从0递增
class Mybitset {
public:
	uint8_t bits[len];
	Mybitset() {
		reset();
	}
	//置位
	void set(int pos, bool value) {
		if (value == 1)
			bits[len - 1 - pos / 8] |= value << (pos % 8);
		else
			bits[len - 1 - pos / 8] &= (~(value << (pos % 8)));
	}
	//判断是否全0
	bool none() {
		for (int i = 0; i < len; i++)
			if (bits[i] != 0)
				return false;
		return true;
	}
	//首项（从左向右首个为1的bit位）
	int lp() {
		if (none()) return -1;
		int i = -1;
		while (i < len - 1 && bits[++i] == 0);
		int j = 7;
		for (; j >= 0 && ((bits[i] >> j) & 1) == 0; j--);
		return (len - 1 - i) * 8 + j;
	}
	//全部置0
	void reset() {
		for (int i = 0; i < len; i++)
			bits[i] = 0;
	}
	//判断是否相等
	bool equal(Mybitset another) {
		for (int i = 0; i < len; i++)
			if (bits[i] != another.bits[i])
				return false;
		return true;
	}
}R[N], E[m];//R:所有消元子构成的集合，R[i]:首项为i的消元子; E:所有被消元行构成的数组 


//转义为01串
void set() {
	string line;
	for (int i = 0; i < m; i++) E[i].reset();
	for (int i = 0; i < N; i++) R[i].reset();
	//读入消元子
	ifstream  input_elimination(elimination_txt);
	while (getline(input_elimination, line)) {
		stringstream ss;
		ss << line;
		int lp;
		ss >> lp;
		R[lp].set(lp, 1);
		if (!ss.eof()) {
			int pos;
			while (ss >> pos)
				R[lp].set(pos, 1);
		}
	}
	input_elimination.close();
	//读入被消元行
	ifstream input_deleted_row(deleted_row_txt);
	for (int i = 0; i < m; i++) {
		getline(input_deleted_row, line);
		stringstream ss;
		ss << line;
		int pos;
		while (ss >> pos)
			E[i].set(pos, 1);
	}
	input_deleted_row.close();
}

//特殊高斯消去法串行实现
void Serial() {
	for (int k = 0; k < m; k++) {
		while (!E[k].none()) {
			int E_k_lp = E[k].lp();//取被消元行E[k]首项
			bool have_Rlp = !R[E_k_lp].none();//是否有相应消元子
			if (have_Rlp)
				//串行异或
				for (int i = 0; i < len; i++)
					E[k].bits[i] ^= R[E_k_lp].bits[i];
			else {
				R[E_k_lp] = E[k];//被消元行升格为消元子
				break;
			}
		}
	}
}

//静态线程数据结构定义
struct Static_threadParam_t {
	int t_id;//线程id
};

//barrier定义
pthread_barrier_t barrier_begin;
pthread_barrier_t barrier_EOR_end;

//特殊高斯消去法 Pthread多线程异或 静态线程+barrier同步+全部循环纳入线程函数
void* Static_barrier_threadFunc(void* param)
{
	Static_threadParam_t* p = (Static_threadParam_t*)param;
	int t_id = p->t_id;
	for (int k = 0; k < m; k++) {
		while (!E[k].none()) {
			int E_k_lp = E[k].lp();
			bool have_Rlp = !R[E_k_lp].none();
			//第一个barrier同步：各线程一起进入分支语句
			//一方面，if分支的异或处理会导致首项的改变，所以所有线程都先根据首项判断是否有对应消元行
			//另一方面，else分支的break会跳出while循环，开始对下一行被消元行进行处理，对下一行处理之前需要先同步
			//所以，这个barrier放在两个分支之前。
			pthread_barrier_wait(&barrier_begin);
			if (have_Rlp) {
				//对异或过程循环划分
				for (int i = t_id; i < len; i += NUM_THREADS) {
					E[k].bits[i] ^= R[E_k_lp].bits[i];
				}
				//等待该次各个异或线程均结束后，所有线程一起进入下一轮
				//如果该轮各个线程异或结束后E[k]变为全0，本应跳出while循环，但不同步结束可能出现某些线程提前进入下一个while循环的异常情况
				pthread_barrier_wait(&barrier_EOR_end);
			}
			else {
				if (t_id == 0)	R[E_k_lp] = E[k];
				break;
			}
		}
	}
	pthread_exit(NULL);
	return NULL;
}

void Static_barrier() {
	//初始化barrier
	pthread_barrier_init(&barrier_begin, NULL, NUM_THREADS);
	pthread_barrier_init(&barrier_EOR_end, NULL, NUM_THREADS);
	//创建线程
	pthread_t thread_handles[NUM_THREADS];// 创建对应的 Handle
	Static_threadParam_t param[NUM_THREADS];// 创建对应的线程数据结构
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)
	{
		param[t_id].t_id = t_id;
		pthread_create(&thread_handles[t_id], NULL, Static_barrier_threadFunc, (void*)&param[t_id]);
	}
	for (int t_id = 0; t_id < NUM_THREADS; t_id++)	pthread_join(thread_handles[t_id], NULL);

	pthread_barrier_destroy(&barrier_begin);
	pthread_barrier_destroy(&barrier_EOR_end);
}

//对比自己的算法的消元结果和正确的结果，验证算法正确性
void verify() {
	Mybitset result[m];//正确的消元结果，用于检验
	ifstream input_result_txt(result_txt);
	string line;
	for (int i = 0; i < m; i++) {
		stringstream ss;
		getline(input_result_txt, line);
		ss << line;
		if (!ss.eof()) {
			int pos;
			while (ss >> pos)
				result[i].set(pos, 1);
		}
	}
	for (int i = 0; i < m; i++) {
		if (!E[i].equal(result[i])) {
			cout << "error" << endl;
			exit(2);
		}
	}
	input_result_txt.close();
}

//主函数对以上各种特殊高斯消去法的函数分别计时，采用重复测试方法
int main() {
	do {
		set();
		timespec_get(&sts, TIME_UTC);
		Serial();
		timespec_get(&ets, TIME_UTC);
		verify();
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);//25ms
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("Serial(average): %ld.%09llds\n", dsec, dnsec);
	counter = 0; dsec = 0; dnsec = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		Static_barrier();
		timespec_get(&ets, TIME_UTC);
		verify();
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);//25ms
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("Static_barrier(average): %ld.%09llds\n", dsec, dnsec);
	
}