/*普通高斯消去 串行算法和MPI按行静态循环划分算法的对比，兼容arm、x86平台*/
#include<stdio.h>
#include<cmath>
#include<chrono>

#include<mpi.h>

const int N_[10] = { 16, 32, 64, 128, 256, 512, 1024, 2048, 3072, 4096 };//矩阵行/列数
const int total_count[10] = { 10000, 1000, 200, 25, 3, 1, 1, 1, 1, 1 };//测量总次数
float m[4096][4096];

//构造矩阵
void set(int N) {
	const int MIN = -2, MAX = 2;//随机数生成范围，设的比较小，防止初始化以及计算中经过多次行变换后数据溢出
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < i; j++)
			m[i][j] = 0.0;
		m[i][i] = 1.0;
		for (int j = i + 1; j < N; j++) {
			m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
			while (-0.1 < m[i][j]&& m[i][j] < 0.1)
				m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
		}
	}
	//每个循环随机取两行，其中一行加上另一行
	for (int k = 0; k < N * N; k++) {
		int i1 = rand() % N, i2 = rand() % N;
		//先检验两行相加后是否有较大或较小的值，再相加
		bool overflow = false;
		for (int j = 0; j < N; j++)
			if (m[i2][j] + m[i1][j] > 2000 ||(-0.04 < m[i2][j] + m[i1][j] && m[i2][j] + m[i1][j] < 0.04)) { overflow = true; break; }
		if (!overflow)
			for (int j = 0; j < N; j++)
				m[i2][j] = m[i2][j] + m[i1][j];
	}
}
//串行算法
void Serial(int N) {
	for (int k = 0; k < N; k++) {
		for (int j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			for (int j = k + 1; j < N; j++)
				m[i][j] -= m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

int main(int argc, char* argv[]) {
	int rank, NUM_PROCS;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &NUM_PROCS);
	//测试不同矩阵规模下串行算法的平均执行时间
	if (rank == 0) {
		for (int p = 0; p < 10; p++) {
			int N = N_[p];
			double total_time = 0, counter = 0;
			do {
				set(N);
				auto start = std::chrono::high_resolution_clock::now();

				Serial(N);

				auto end = std::chrono::high_resolution_clock::now();
				total_time += std::chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
			} while (counter < total_count[p]);
			printf("Serial: N = %d, Average_Time = %.9lf s\n", N, total_time / counter);
		}
	}
	//测试不同矩阵规模下MPI算法的平均执行时间
	for (int p = 0; p < 10; p++) {
		int N = N_[p];
		double total_time = 0, counter = 0;
		do {
			MPI_Barrier(MPI_COMM_WORLD);
			if (rank == 0) {
				set(N);
				for (int id = 1; id < NUM_PROCS; id++)
					for (int i = id; i < N; i += NUM_PROCS)
						MPI_Send(&m[i][0], N, MPI_FLOAT, id, i, MPI_COMM_WORLD);
			}
			else {
				for (int i = rank; i < N; i += NUM_PROCS)
					MPI_Recv(&m[i][0], N, MPI_FLOAT, 0, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			}


			MPI_Barrier(MPI_COMM_WORLD);
			double start = MPI_Wtime();

			for (int k = 0; k < N; k++) {
				if (rank == k % NUM_PROCS) {
					for (int j = k + 1; j < N; j++)
						m[k][j] /= m[k][k];
					m[k][k] = 1.0;
				}
				MPI_Bcast(&m[k][k], N - k, MPI_FLOAT, k % NUM_PROCS, MPI_COMM_WORLD);//如果像这样不传一整行，消元后的矩阵的下三角部分会出现非零值，忽略即可
				for (int i = rank + ceil((float)(k + 1 - rank) / NUM_PROCS) * NUM_PROCS; i < N; i += NUM_PROCS) {
					for (int j = k + 1; j < N; j++)
						m[i][j] -= m[i][k] * m[k][j];
					m[i][k] = 0.0;
				}
			}

			MPI_Barrier(MPI_COMM_WORLD);
			double end = MPI_Wtime();
			total_time += end - start, counter++;
		} while (counter < total_count[p]);

		if (rank == 0) {
			printf("MPI: N = %d, NUM_PROCS = %d, Average_Time = %.9lf s\n", N, NUM_PROCS, total_time / counter);
		}
	}
	MPI_Finalize();

	return 0;
}




