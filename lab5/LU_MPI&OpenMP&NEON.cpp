/*普通高斯消去 MPI静态行循环划分+OpenMP行循环划分+NEON向量化，arm平台*/
#include<stdio.h>
#include<cmath>

#include<mpi.h>
#include<omp.h>
#include<arm_neon.h>

const int N_[10] = { 16, 32, 64, 128, 256, 512, 1024, 2048, 3072, 4096 };//矩阵行/列数
const int total_count[10] = { 10000, 1000, 200, 25, 3, 1, 1, 1, 1, 1 };//不同规模下测试总次数
const int NUM_THREADS_[3] = { 2, 3, 4 };//线程数
float m[4096][4096];

//构造矩阵
void set(int N) {
	const int MIN = -2, MAX = 2;//随机数生成范围，设的比较小，防止初始化以及计算中经过多次行变换后数据溢出
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < i; j++)
			m[i][j] = 0.0;
		m[i][i] = 1.0;
		for (int j = i + 1; j < N; j++) {
			m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
			while (m[i][j] == 0)
				m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
		}
	}
	//每个循环随机取两行，其中一行加上另一行
	for (int k = 0; k < N * N; k++) {
		int i1 = rand() % N, i2 = rand() % N;
		//先检验两行相加后是否有较大的值，再相加
		bool overflow = false;
		for (int j = 0; j < N; j++)
			if (m[i2][j] + m[i1][j] > 10000) { overflow = true; break; }
		if (!overflow)
			for (int j = 0; j < N; j++)
				m[i2][j] = m[i2][j] + m[i1][j];
	}
}

int main(int argc, char* argv[]) {
	//测试不同矩阵规模、不同线程数下MPI+OpenMP+NEON算法的平均执行时间
	int rank, NUM_PROCS;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &NUM_PROCS);
	for (int p = 0; p < 10; p++) {
		for (int q = 0; q < 3; q++) {
			int N = N_[p], NUM_THREADS = NUM_THREADS_[q];
			double total_time = 0, counter = 0;
			do {
				MPI_Barrier(MPI_COMM_WORLD);
				if (rank == 0) 
					set(N);
				for (int i = 0; i < N; i++)
					MPI_Bcast(&m[i][0], N, MPI_FLOAT, 0, MPI_COMM_WORLD);
				MPI_Barrier(MPI_COMM_WORLD);
				double start = MPI_Wtime();

				int i, j, k;
				float32x4_t vkk, vkj, vik, vij;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k,vkk,vkj,vik,vij)
				for (k = 0; k < N; k++) {
#pragma omp single
					{
						if (rank == k % NUM_PROCS) {
							vkk = vld1q_dup_f32(&m[k][k]);
							for (j = k + 1; j + 4 <= N; j += 4) {
								vkj = vld1q_f32(&m[k][j]);
								vkj = vdivq_f32(vkj, vkk);
								vst1q_f32(&m[k][j], vkj);
							}
							for (; j < N; j++) {
								m[k][j] /= m[k][k];
							}
							m[k][k] = 1.0;
						}
						MPI_Bcast(&m[k][k], N - k, MPI_FLOAT, k % NUM_PROCS, MPI_COMM_WORLD);
					}
#pragma omp for
					for (i = rank + ceil((float)(k + 1 - rank) / NUM_PROCS) * NUM_PROCS; i < N; i += NUM_PROCS) {
						vik = vld1q_dup_f32(&m[i][k]);
						for (j = k + 1; j + 4 <= N; j += 4) {
							vkj = vld1q_f32(&m[k][j]);
							vij = vld1q_f32(&m[i][j]);
							vij = vsubq_f32(vij, vmulq_f32(vik, vkj));
							vst1q_f32(&m[i][j], vij);
						}
						for (; j < N; j++)
							m[i][j] -= m[i][k] * m[k][j];
						m[i][k] = 0;
					}
				}

				MPI_Barrier(MPI_COMM_WORLD);
				double end = MPI_Wtime();
				total_time += end - start, counter++;
			} while (counter < total_count[p]);

			if (rank == 0) {
				printf("MPI + OpenMP + NEON: N = %d, NUM_PROCS = %d, NUM_THREADS = %d, Average_Time = %.9lf s\n", N, NUM_PROCS, NUM_THREADS, total_time / counter);
			}
		}
	}
	MPI_Finalize();

	return 0;
}
