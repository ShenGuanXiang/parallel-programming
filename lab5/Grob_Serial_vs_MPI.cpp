/*特殊高斯消去 串行和MPI行循环划分对比，兼容arm、x86平台*/
#include<stdio.h>
#include<cstring>
#include<fstream>
#include<sstream>
#include<chrono>
#include<cmath>

#include<mpi.h>
using namespace std;

const int N_[11] = { 130, 254, 562, 1011, 2362, 3799, 8399, 23045, 37960, 43577, 85401 };//矩阵列数
const int m_[11] = { 8, 53, 53, 263, 453, 1953, 4535, 14325, 14921, 54274, 756 };//被消元行总行数
const int total_count[11] = { 3000, 40, 25, 25, 1, 1, 1, 1, 1, 1 ,1 };//测量总次数
string elimination_txt[11] = { "1_130_22_8//1.txt", "2_254_106_53//1.txt", "3_562_170_53//1.txt", "4_1011_539_263//1.txt", "5_2362_1226_453//1.txt", "6_3799_2759_1953//1.txt", "7_8399_6375_4535//1.txt", "8_23045_18748_14325//1.txt", "9_37960_29304_14921//1.txt" };//消元子文件路径
string deleted_row_txt[11] = { "1_130_22_8//2.txt", "2_254_106_53//2.txt", "3_562_170_53//2.txt", "4_1011_539_263//2.txt", "5_2362_1226_453//2.txt", "6_3799_2759_1953//2.txt", "7_8399_6375_4535//2.txt", "8_23045_18748_14325//2.txt", "9_37960_29304_14921//2.txt" };//被消元行文件路径
string result_txt[11] = { "1_130_22_8//3.txt", "2_254_106_53//3.txt", "3_562_170_53//3.txt", "4_1011_539_263//3.txt", "5_2362_1226_453//3.txt", "6_3799_2759_1953//3.txt", "7_8399_6375_4535//3.txt", "8_23045_18748_14325//3.txt", "9_37960_29304_14921//3.txt" };//消元结果文件路径
const int len_[11] = { 5, 8, 18, 32, 74, 119, 263, 721, 1187, 1362, 2669 };//Mybitset中bits数组元素个数(N/32向上取整)

//bit位从右（高地址）向左（低地址）开始从0递增
class Mybitset {
public:
	int len = 0;
	int* bits = NULL;
	Mybitset() {
		len = 0;
	}
	Mybitset(int len) {
		this->len = len;
		bits = new int[len];
		reset();
	}
	~Mybitset() {
		if (bits != NULL) {
			delete[]bits;
			bits = NULL;
		}
	}
	//置位
	void set(int pos, bool value) {
		if (value == 1)
			bits[len - 1 - pos / 32] |= value << (pos % 32);
		else
			bits[len - 1 - pos / 32] &= (~(value << (pos % 32)));
	}
	//获取一位的值
	bool get(int pos) {
		return (bits[len - 1 - pos / 32]) & (1 << (pos % 32));
	}
	//首项（从左向右首个为1的bit位）
	int lp() {
		if (none()) return -1;
		int i = -1;
		while (i < len - 1 && bits[++i] == 0);
		int j = 31;
		for (; j >= 0 && ((bits[i] >> j) & 1) == 0; j--);
		return (len - 1 - i) * 32 + j;
	}
	//判断是否全0
	bool none() {
		for (int i = 0; i < len; i++)
			if (bits[i] != 0)
				return false;
		return true;
	}
	//全部置0
	void reset() {
		for (int i = 0; i < len; i++)
			bits[i] = 0;
	}

	//判断是否相等
	bool equal(Mybitset* another) {
		for (int i = 0; i < len; i++)
			if (bits[i] != another->bits[i])
				return false;
		return true;
	}
	void print() {
		for (int i = 0; i < len; i++)
			for (int j = 0; j < 32; j++)
				printf("%d", ((bits[i] >> (31 - j)) & 1) != 0);
		printf("\n");
	}
};

Mybitset** R = NULL;//R:所有消元子构成的集合，R[i]:首项为i的消元子, NULL代表没有对应消元子; 
Mybitset** E = NULL;//E:所有被消元行构成的数组 

//转义为01串
void set(int SampleId) {
	int N = N_[SampleId], m = m_[SampleId], len = len_[SampleId];
	if (E != NULL) {
		for (int i = 0; i < m; i++)
			if (E[i] != NULL) {
				if (R != NULL && !E[i]->none())
					R[E[i]->lp()] = NULL;
				delete E[i];
				E[i] = NULL;
			}
		delete[] E;
		E = NULL;
	}
	if (R != NULL) {
		for (int i = 0; i < N; i++)
			if (R[i] != NULL) {
				delete R[i];
				R[i] = NULL;
			}
		delete[]R;
		R = NULL;
	}
	E = new Mybitset * [m];
	for (int i = 0; i < m; i++) {
		E[i] = new Mybitset(len);
	}
	R = new Mybitset * [N];
	for (int i = 0; i < N; i++) {
		R[i] = NULL;
	}
	string line;
	//读入消元子
	ifstream  input_elimination(elimination_txt[SampleId]);
	while (getline(input_elimination, line)) {
		stringstream ss;
		ss << line;
		int lp;
		ss >> lp;
		if (R[lp] != NULL) {
			delete R[lp];
			R[lp] = NULL;
		}
		R[lp] = new Mybitset(len);
		R[lp]->set(lp, 1);
		if (!ss.eof()) {
			int pos;
			while (ss >> pos)
				R[lp]->set(pos, 1);
		}
	}
	input_elimination.close();
	//读入被消元行
	ifstream input_deleted_row(deleted_row_txt[SampleId]);
	for (int i = 0; i < m; i++) {
		getline(input_deleted_row, line);
		stringstream ss;
		ss << line;
		int pos;
		while (ss >> pos)
			E[i]->set(pos, 1);
	}
	input_deleted_row.close();
}

void destroy(int m, int N) {
	if (E != NULL) {
		for (int i = 0; i < m; i++)
			if (E[i] != NULL) {
				if (R != NULL && !E[i]->none())
					R[E[i]->lp()] = NULL;
				delete E[i];
				E[i] = NULL;
			}
		delete[]E;
		E = NULL;
	}
	if (R != NULL) {
		for (int i = 0; i < N; i++)
			if (R[i] != NULL) {
				delete R[i];
				R[i] = NULL;
			}
		delete[]R;
		R = NULL;
	}
}

//对比自己的算法的消元结果和正确的结果，验证算法正确性
void verify(int SampleId) {
	int m = m_[SampleId];
	Mybitset** result = new Mybitset * [m];//正确的消元结果，用于检验
	for (int i = 0; i < m; i++)
		result[i] = new Mybitset(len_[SampleId]);
	ifstream input_result_txt(result_txt[SampleId]);
	string line;
	for (int i = 0; i < m; i++) {
		stringstream ss;
		getline(input_result_txt, line);
		ss << line;
		if (!ss.eof()) {
			int pos;
			while (ss >> pos)
				result[i]->set(pos, 1);
		}
	}
	for (int i = 0; i < m; i++) {
		if (!E[i]->equal(result[i])) {
			printf("error!\n");
			exit(2);
		}
	}
	input_result_txt.close();
	for (int i = 0; i < m; i++) {
		delete result[i];
		result[i] = NULL;
	}
	delete[]result;
	result = NULL;
}

void Serial(int N, int m, int len) {
	bool* f = new bool[m];//对应的被消元行是否被升格为消元子
	memset(f, false, m * sizeof(bool));
	for (int k = N - 1; k >= 0; k--) {
		//没有对应消元子，则被消元行升格为消元子
		if (R[k] == NULL) {
			for (int i = 0; i < m; i++) {
				if (E[i]->get(k) && !f[i]) {
					R[k] = E[i];
					f[i] = true;
					break;
				}
			}
		}
		if (R[k] != NULL) {
			for (int i = 0; i < m; i++) {
				if (E[i]->get(k) && !f[i]) {
					for (int j = 0; j < len; j++)
						E[i]->bits[j] ^= R[k]->bits[j];
				}
			}
		}
	}
	delete[]f;
}

int main(int argc, char* argv[]) {
	
	int rank, NUM_PROCS;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &NUM_PROCS);
	//测试不同样本串行算法的平均执行时间
	if (rank == 0) {
		for (int SampleId = 0; SampleId < 6; SampleId++) {
			int N = N_[SampleId], m = m_[SampleId], len = len_[SampleId];
			double total_time = 0, counter = 0;
			do {
				set(SampleId);
				auto start = chrono::high_resolution_clock::now();

				Serial(N, m, len);

				auto end = chrono::high_resolution_clock::now();
				total_time += chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
				verify(SampleId);
			} while (counter < total_count[SampleId]);
			printf("Serial: Sample%d Average_Time = %.9lf s\n", SampleId + 1, total_time / counter);
			destroy(m, N);
		}
	}
	//测试不同样本MPI算法的平均执行时间
	for (int SampleId = 0; SampleId < 6; SampleId++) {
		int N = N_[SampleId], m = m_[SampleId], len = len_[SampleId];

		//MPI_Datatype MPI_Mybitset;
		//int block_lengths[2] = { 1,len };
		//MPI_Aint displacements[2];
		//MPI_Aint addresses[2], add_start;
		//MPI_Datatype typelist[2] = { MPI_INT, MPI_INT };
		//Mybitset temp(len);
		//MPI_Get_address(&temp, &add_start);
		//MPI_Get_address(&temp.len, &addresses[0]);
		//MPI_Get_address(&temp.bits, &addresses[1]);
		//displacements[0] = addresses[0] - add_start;
		//displacements[1] = addresses[1] - add_start;
		//MPI_Type_create_struct(2, block_lengths, displacements, typelist, &MPI_Mybitset);
		//MPI_Type_commit(&MPI_Mybitset);

		double total_time = 0, counter = 0;
		do {
			set(SampleId);

			bool* f = new bool[m];
			memset(f, false, m * sizeof(bool));

			MPI_Barrier(MPI_COMM_WORLD);
			double start = MPI_Wtime();

			for (int k = N - 1; k >= 0; k--) {
				int line_to_upgrade = m, line_to_upgrade_final = m;
				if (R[k] == NULL) {
					for (int i = rank; i < m; i += NUM_PROCS) {
						if (E[i]->get(k) && !f[i]) {
							line_to_upgrade = i;
							break;
						}
					}
					//MPI_Barrier(MPI_COMM_WORLD);
					MPI_Reduce(&line_to_upgrade, &line_to_upgrade_final, 1, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
					MPI_Bcast(&line_to_upgrade_final, 1, MPI_INT, 0, MPI_COMM_WORLD);
				}
				if (line_to_upgrade != m) {
					R[k] = E[line_to_upgrade_final];
					f[line_to_upgrade_final] = true;
				}
				if (R[k] != NULL) {
					for (int i = rank; i < m; i += NUM_PROCS) {
						if (E[i]->get(k) && !f[i]) {
							for (int j = 0; j < len; j++)
								E[i]->bits[j] ^= R[k]->bits[j];
						}
					}
				}
			}


			MPI_Barrier(MPI_COMM_WORLD);
			double end = MPI_Wtime();
			total_time += end - start, counter++;

			if (rank == 0)
				for (int id = 1; id < NUM_PROCS; id++)
					for (int i = id; i < m; i += NUM_PROCS)
						MPI_Recv(E[i]->bits, len, MPI_INT, id, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			else
				for (int i = rank; i < m; i += NUM_PROCS)
					MPI_Send(E[i]->bits, len, MPI_INT, 0, i, MPI_COMM_WORLD);

		/*	if (rank == 0) {
				for (int i = 0; i < m; i++) {
					E[i]->print();
				}
				verify(SampleId);
			}*/
			delete[]f;
		} while (counter < total_count[SampleId]);

		if (rank == 0) {
			printf("MPI: Sample%d NUM_PROCS = %d Average_Time = %.9lf s\n", SampleId + 1, NUM_PROCS, total_time / counter);
		}

		//MPI_Type_free(&MPI_Mybitset);
	}
	MPI_Finalize();

	return 0;

}