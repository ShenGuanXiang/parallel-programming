#include <stdio.h>
#include <time.h>
#include <random>
#include <iostream>
#include <nmmintrin.h> //SSE4.2
#include <immintrin.h> //AVX、AVX2
#include <cstring>
#define MIN  -1
#define MAX   1
using namespace std;

const int N = 20;//矩阵行数兼列数
float** m = NULL;//输入矩阵

struct timespec sts, ets;
time_t dsec;
long long dnsec;
int counter;//重复测试计数器
long long tottime;//每次重复测试的总时间


void set() {
	srand(static_cast <unsigned> (time(0)));
	srand((unsigned)time(NULL));
	m = new float* [N];
	for (int i = 0; i < N; i++) 
		m[i] = new float[N];
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < i; j++)
			m[i][j] = 0;
		m[i][i] = 1.0;
		for (int j = i + 1; j < N; j++)
			m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
	}
	for (int t = 0; t < N * N; t++) {
		int i1 = rand() % N, i2 = rand() % N;
		for (int j = 0; j < N; j++) 
			m[i2][j] += m[i1][j];
	}
}

void destroy() {
	for (int i = 0; i < N; i++) 
		delete[]m[i];
	delete[]m;
}

void LU_Serial() {
	for (int k = 0; k < N; k++) {
		for (int j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			for (int j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

void LU_SSE_unaligned() {
	for (int k = 0; k < N; k++) {
		__m128 vkk = _mm_set1_ps(m[k][k]);
		int j = k + 1;
		for (; j + 4 <= N; j += 4) {
			__m128 vkj = _mm_loadu_ps(&m[k][j]);
			vkj = _mm_div_ps(vkj, vkk);
			_mm_storeu_ps(&m[k][j], vkj);
		}
		for (; j < N; j++) {
			m[k][j] = m[k][j] / m[k][k];
		}
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			__m128 vik = _mm_set1_ps(m[i][k]);
			int j = k + 1;
			for (; j + 4 <= N; j += 4) {
				__m128 vkj = _mm_loadu_ps(&m[k][j]);
				__m128 vij = _mm_loadu_ps(&m[i][j]);
				vij = _mm_sub_ps(vij, _mm_mul_ps(vik, vkj));
				_mm_storeu_ps(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

void LU_AVX_unaligned() {
	for (int k = 0; k < N; k++) {
		__m256 vkk = _mm256_set1_ps(m[k][k]);
		int j = k + 1;
		for (; j + 8 <= N; j += 8) {
			__m256 vkj = _mm256_loadu_ps(&m[k][j]);
			vkj = _mm256_div_ps(vkj, vkk);
			_mm256_storeu_ps(&m[k][j], vkj);
		}
		for (; j < N; j++) {
			m[k][j] = m[k][j] / m[k][k];
		}
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			__m256 vik = _mm256_set1_ps(m[i][k]);
			int j = k + 1;
			for (; j + 8 <= N; j += 8) {
				__m256 vkj = _mm256_loadu_ps(&m[k][j]);
				__m256 vij = _mm256_loadu_ps(&m[i][j]);
				vij = _mm256_sub_ps(vij, _mm256_mul_ps(vik, vkj));
				_mm256_storeu_ps(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

void LU_AVX512_unaligned() {
	for (int k = 0; k < N; k++) {
		__m512 vkk = _mm512_set1_ps(m[k][k]);
		int j = k + 1;
		for (; j + 16 <= N; j += 16) {
			__m512 vkj = _mm512_loadu_ps(&m[k][j]);
			vkj = _mm512_div_ps(vkj, vkk);
			_mm512_storeu_ps(&m[k][j], vkj);
		}
		for (; j < N; j++) {
			m[k][j] = m[k][j] / m[k][k];
		}
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			__m512 vik = _mm512_set1_ps(m[i][k]);
			int j = k + 1;
			for (; j + 16 <= N; j += 16) {
				__m512 vkj = _mm512_loadu_ps(&m[k][j]);
				__m512 vij = _mm512_loadu_ps(&m[i][j]);
				vij = _mm512_sub_ps(vij, _mm512_mul_ps(vik, vkj));
				_mm512_storeu_ps(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}



void LU_SSE_unaligned_onlyLoop1() {
	for (int k = 0; k < N; k++) {
		__m128 vkk = _mm_set1_ps(m[k][k]);
		int j = k + 1;
		for (; j + 4 <= N; j += 4) {
			__m128 vkj = _mm_loadu_ps(&m[k][j]);
			vkj = _mm_div_ps(vkj, vkk);
			_mm_storeu_ps(&m[k][j], vkj);
		}
		for (; j < N; j++) {
			m[k][j] = m[k][j] / m[k][k];
		}
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			for (int j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

void LU_SSE_unaligned_onlyLoop2() {
	for (int k = 0; k < N; k++) {
		for (int j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			__m128 vik = _mm_set1_ps(m[i][k]);
			int j = k + 1;
			for (; j + 4 <= N; j += 4) {
				__m128 vkj = _mm_loadu_ps(&m[k][j]);
				__m128 vij = _mm_loadu_ps(&m[i][j]);
				vij = _mm_sub_ps(vij, _mm_mul_ps(vik, vkj));
				_mm_storeu_ps(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

void LU_AVX_unaligned_onlyLoop1() {
	for (int k = 0; k < N; k++) {
		__m256 vkk = _mm256_set1_ps(m[k][k]);
		int j = k + 1;
		for (; j + 8 <= N; j += 8) {
			__m256 vkj = _mm256_loadu_ps(&m[k][j]);
			vkj = _mm256_div_ps(vkj, vkk);
			_mm256_storeu_ps(&m[k][j], vkj);
		}
		for (; j < N; j++) {
			m[k][j] = m[k][j] / m[k][k];
		}
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			for (int j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

void LU_AVX_unaligned_onlyLoop2() {
	for (int k = 0; k < N; k++) {
		__m256 vkk = _mm256_set1_ps(m[k][k]);
		for (int j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			__m256 vik = _mm256_set1_ps(m[i][k]);
			int j = k + 1;
			for (; j + 8 <= N; j += 8) {
				__m256 vkj = _mm256_loadu_ps(&m[k][j]);
				__m256 vij = _mm256_loadu_ps(&m[i][j]);
				vij = _mm256_sub_ps(vij, _mm256_mul_ps(vik, vkj));
				_mm256_storeu_ps(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}


//main函数中对以上几个函数分别重复测试计时
int main() {
	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_Serial();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_Serial(average): %ld.%09llds\n", dsec, dnsec);
	counter = 0; dsec = 0; dnsec = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_SSE_unaligned();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_SSE_unaligned(average): %ld.%09llds\n", dsec, dnsec);
	counter = 0; dsec = 0; dnsec = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_AVX_unaligned();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_AVX_unaligned(average): %ld.%09llds\n", dsec, dnsec);
	counter = 0; dsec = 0; dnsec = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_AVX512_unaligned();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_AVX512_unaligned(average): %ld.%09llds\n", dsec, dnsec);
	counter = 0; dsec = 0; dnsec = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_SSE_unaligned_onlyLoop1();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_SSE_unaligned_onlyLoop1(average): %ld.%09llds\n", dsec, dnsec);
	counter = 0; dsec = 0; dnsec = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_SSE_unaligned_onlyLoop2();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_SSE_unaligned_onlyLoop2(average): %ld.%09llds\n", dsec, dnsec);
	counter = 0; dsec = 0; dnsec = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_AVX_unaligned_onlyLoop1();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_AVX_unaligned_onlyLoop1(average): %ld.%09llds\n", dsec, dnsec);
	counter = 0; dsec = 0; dnsec = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_AVX_unaligned_onlyLoop2();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_AVX_unaligned_onlyLoop2(average): %ld.%09llds\n", dsec, dnsec);
	counter = 0; dsec = 0; dnsec = 0;
}