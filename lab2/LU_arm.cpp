#include <iostream>
#include <random>
#include <time.h>
#include <stdio.h>
#include <arm_neon.h>
#include <cstring>
//伪随机浮点数范围
#define MIN -1.0
#define MAX 1.0
using namespace std;

const int N = 20;//矩阵的行数兼列数
float** m = NULL;//默认8字节对齐
float** m_aligned = NULL;//设为16字节对齐

struct timespec sts, ets;
time_t dsec;
long long dnsec;
int counter;//重复测试计数器
long long tottime;//每次重复测试的总时间

//初始化m、m_aligned
void set() {
	srand(static_cast <unsigned> (time(0)));
	srand((unsigned)time(NULL));
	m = new float* [N];
	m_aligned = new float* [N];
	for (int i = 0; i < N; i++) {
		m[i] = new float[N];
		m_aligned[i] = (float*)aligned_alloc(16, N * sizeof(float) * 16);
	}
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < i; j++)
			m[i][j] = 0;
		m[i][i] = 1.0;
		for (int j = i + 1; j < N; j++)
			m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
	}
	for (int k = 0; k < N; k++)
		for (int i = k + 1; i < N; i++) {
			int t = (-1 + rand() % 2);
			for (int j = 0; j < N; j++)
				m[i][j] += t * m[k][j];
		}
	for (int i = 0; i < N; i++)
		memcpy(m_aligned[i], m[i], N * sizeof(float));
}

//回收m、m_aligned
void destroy() {
	for (int i = 0; i < N; i++) {
		if (m[i] != nullptr) delete[]m[i];
		if (m_aligned[i] != nullptr) free(m_aligned[i]);
	}
	delete[]m;
	delete[]m_aligned;
}

void LU_Serial() {
	for (int k = 0; k < N; k++) {
		for (int j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			for (int j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

void LU_NEON_unaligned() {
	for (int k = 0; k < N; k++) {
		float32x4_t vkk = vld1q_dup_f32(&m[k][k]);
		int j = k + 1;
		for (; j + 4 <= N; j += 4) {
			float32x4_t vkj = vld1q_f32(&m[k][j]);
			vkj = vdivq_f32(vkj, vkk);
			vst1q_f32(&m[k][j], vkj);
		}
		for (; j < N; j++) {
			m[k][j] = m[k][j] / m[k][k];
		}
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			float32x4_t vik = vld1q_dup_f32(&m[i][k]);
			int j = k + 1;
			for (; j + 4 <= N; j += 4) {
				float32x4_t vkj = vld1q_f32(&m[k][j]);
				float32x4_t vij = vld1q_f32(&m[i][j]);
				vij = vsubq_f32(vij, vmulq_f32(vik, vkj));
				vst1q_f32(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

//只对除法并行
void LU_NEON_unaligned_onlyloop1() {
	for (int k = 0; k < N; k++) {
		float32x4_t vkk = vld1q_dup_f32(&m[k][k]);
		int j = k + 1;
		for (; j + 4 <= N; j += 4) {
			float32x4_t vkj = vld1q_f32(&m[k][j]);
			vkj = vdivq_f32(vkj, vkk);
			vst1q_f32(&m[k][j], vkj);
		}
		for (; j < N; j++) {
			m[k][j] = m[k][j] / m[k][k];
		}
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			for (int j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

//只对消去并行
void LU_NEON_unaligned_onlyloop2() {
	for (int k = 0; k < N; k++) {
		for (int j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			float32x4_t vik = vld1q_dup_f32(&m[i][k]);
			int j = k + 1;
			for (; j + 4 <= N; j += 4) {
				float32x4_t vkj = vld1q_f32(&m[k][j]);
				float32x4_t vij = vld1q_f32(&m[i][j]);
				vij = vsubq_f32(vij, vmulq_f32(vik, vkj));
				vst1q_f32(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

void LU_NEON_aligned() {
	for (int k = 0; k < N; k++) {
		float32x4_t vkk = vld1q_dup_f32(&m_aligned[k][k]);
		int j = k + 1;
		for (; j + 4 <= N; j += 4) {
			float32x4_t vkj = vld1q_f32(&m_aligned[k][j]);
			vkj = vdivq_f32(vkj, vkk);
			vst1q_f32(&m_aligned[k][j], vkj);
		}
		for (; j < N; j++) {
			m_aligned[k][j] = m_aligned[k][j] / m_aligned[k][k];
		}
		m_aligned[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			float32x4_t vik = vld1q_dup_f32(&m_aligned[i][k]);
			int j = k + 1;
			for (; j + 4 <= N; j += 4) {
				float32x4_t vkj = vld1q_f32(&m_aligned[k][j]);
				float32x4_t vij = vld1q_f32(&m_aligned[i][j]);
				vij = vsubq_f32(vij, vmulq_f32(vik, vkj));
				vst1q_f32(&m_aligned[i][j], vij);
			}
			for (; j < N; j++)
				m_aligned[i][j] = m_aligned[i][j] - m_aligned[i][k] * m_aligned[k][j];
			m_aligned[i][k] = 0;
		}
	}
}

void LU_NEON_aligned_onlyloop1() {
	for (int k = 0; k < N; k++) {
		float32x4_t vkk = vld1q_dup_f32(&m_aligned[k][k]);
		int j = k + 1;
		for (; j + 4 <= N; j += 4) {
			float32x4_t vkj = vld1q_f32(&m_aligned[k][j]);
			vkj = vdivq_f32(vkj, vkk);
			vst1q_f32(&m_aligned[k][j], vkj);
		}
		for (; j < N; j++) {
			m_aligned[k][j] = m_aligned[k][j] / m_aligned[k][k];
		}
		m_aligned[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			for (int j = k + 1; j < N; j++)
				m_aligned[i][j] = m_aligned[i][j] - m_aligned[i][k] * m_aligned[k][j];
			m_aligned[i][k] = 0;
		}
	}
}

void LU_NEON_aligned_onlyloop2() {
	for (int k = 0; k < N; k++) {
		for (int j = k + 1; j < N; j++)
			m_aligned[k][j] = m_aligned[k][j] / m_aligned[k][k];
		m_aligned[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			float32x4_t vik = vld1q_dup_f32(&m_aligned[i][k]);
			int j = k + 1;
			for (; j + 4 <= N; j += 4) {
				float32x4_t vkj = vld1q_f32(&m_aligned[k][j]);
				float32x4_t vij = vld1q_f32(&m_aligned[i][j]);
				vij = vsubq_f32(vij, vmulq_f32(vik, vkj));
				vst1q_f32(&m_aligned[i][j], vij);
			}
			for (; j < N; j++)
				m_aligned[i][j] = m_aligned[i][j] - m_aligned[i][k] * m_aligned[k][j];
			m_aligned[i][k] = 0;
		}
	}
}

//对以上各算法重复测试并计算平均执行时间
int main() {
	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_Serial();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_Serial(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0;
	dnsec = 0;
	counter = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_NEON_unaligned();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_NEON_unaligned(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0;
	dnsec = 0;
	counter = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_NEON_unaligned_onlyloop1();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_NEON_unaligned_onlyloop1(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0;
	dnsec = 0;
	counter = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_NEON_unaligned_onlyloop2();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_NEON_unaligned_onlyloop2(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0;
	dnsec = 0;
	counter = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_NEON_aligned();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_NEON_aligned(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0;
	dnsec = 0;
	counter = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_NEON_aligned_onlyloop1();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_NEON_aligned_onlyloop1(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0;
	dnsec = 0;
	counter = 0;

	do {
		set();
		timespec_get(&sts, TIME_UTC);
		LU_NEON_aligned_onlyloop2();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		destroy();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("LU_NEON_aligned_onlyloop2(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0;
	dnsec = 0;
	counter = 0;
}
