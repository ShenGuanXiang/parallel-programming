#include <iostream>
#include <time.h>
#include <stdio.h>
#include <arm_neon.h>
#include <cstring>
#include <fstream>
#include <sstream>
using namespace std;

const int N = 130;//矩阵列数
const int m = 8;//被消元行总行数
string elimination_txt = "1_130_22_8//1.txt", deleted_row_txt = "1_130_22_8//2.txt", result_txt = "1_130_22_8//3.txt";//消元子、被消元行、消元结果文件路径

struct timespec sts, ets;
time_t dsec;
long long dnsec;
int counter;//重复测试计数器
long long tottime;

//bit位从右（高地址）向左（低地址）开始从0递增
class Mybitset {
public:
	uint8_t bits[(N - 1) / 8 + 1];
	int len;
	Mybitset() {
		len = (N - 1) / 8 + 1;//N/8向上取整
		reset();
	}
	//置位
	void set(int pos, bool value) {
		if (value == 1)
			bits[len - 1 - pos / 8] |= value << (pos % 8);
		else
			bits[len - 1 - pos / 8] &= (~(value << (pos % 8)));
	}
	//判断是否全0
	bool none() {
		for (int i = 0; i < len; i++)
			if (bits[i] != 0)
				return false;
		return true;
	}
	//首项（从左向右首个为1的bit位）
	int lp() {
		if (none()) return -1;
		int i = -1;
		while (i < len - 1 && bits[++i] == 0);
		int j = 7;
		for (; j >= 0 && ((bits[i] >> j) & 1) == 0; j--);
		return (len - 1 - i) * 8 + j;
	}
	//串行异或
	void EOR_Serial(Mybitset another) {
		for (int i = 0; i < len; i++)
			bits[i] ^= another.bits[i];
	}
	//并行异或
	void EOR_NEON_unaligned(Mybitset another) {
		int i = 0;
		for (; i + 16 <= len; i += 16) {
			uint8x16_t va = vld1q_u8(&bits[i]);
			uint8x16_t vb = vld1q_u8(&another.bits[i]);
			va = veorq_u8(va, vb);
			vst1q_u8(&bits[i], va);
		}
		for (; i < len; i++)
			bits[i] ^= another.bits[i];
	}
	//全部置0
	void reset() {
		for (int i = 0; i < len; i++)
			bits[i] = 0;
	}
	//判断是否相等
	bool equal(Mybitset another) {
		for (int i = 0; i < len; i++)
			if (bits[i] != another.bits[i])
				return false;
		return true;
	}
}R[N], E[m];//R:所有消元子构成的集合，R[i]:首项为i的消元子; E:所有被消元行构成的数组 

//转义为01串
void set() {
	string line;
	for (int i = 0; i < m; i++) E[i].reset();
	for (int i = 0; i < N; i++) R[i].reset();
	//读入消元子
	ifstream  input_elimination(elimination_txt);
	while (getline(input_elimination, line)) {
		stringstream ss;
		ss << line;
		int lp;
		ss >> lp;
		R[lp].set(lp, 1);
		if (!ss.eof()) {
			int pos;
			while (ss >> pos)
				R[lp].set(pos, 1);
		}
	}
	input_elimination.close();
	//读入被消元行
	ifstream input_deleted_row(deleted_row_txt);
	for (int i = 0; i < m; i++) {
		getline(input_deleted_row, line);
		stringstream ss;
		ss << line;
		int pos;
		while (ss >> pos)
			E[i].set(pos, 1);
	}
	input_deleted_row.close();
}

void GB_Serial() {
	for (int i = 0; i < m; i++) {
		while (!E[i].none()) {
			int E_i_lp = E[i].lp();
			if (!R[E_i_lp].none())
				E[i].EOR_Serial(R[E_i_lp]);
			else {
				R[E_i_lp] = E[i];
				break;
			}
		}
	}
}

void GB_NEON_unaligned() {
	for (int i = 0; i < m; i++) {
		while (!E[i].none()) {
			int E_i_lp = E[i].lp();
			if (!R[E_i_lp].none())
				E[i].EOR_NEON_unaligned(R[E_i_lp]);
			else {
				R[E_i_lp] = E[i];
				break;
			}
		}
	}
}

//验证算法正确性
void verify() {
	Mybitset result[m];//正确的消元结果，用于检验
	ifstream input_result_txt(result_txt);
	string line;
	for (int i = 0; i < m; i++) {
		stringstream ss;
		getline(input_result_txt, line);
		ss << line;
		if (!ss.eof()) {
			int pos;
			while (ss >> pos)
				result[i].set(pos, 1);
		}
	}
	for (int i = 0; i < m; i++) {
		if (!E[i].equal(result[i])) {
			cout << "error" << endl;
			exit(2);
		}
	}
	input_result_txt.close();
}

//主函数对串、并行算法计时
int main() {
	do {
		set();
		timespec_get(&sts, TIME_UTC);
		GB_Serial();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		verify();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("GB_Serial(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0;
	dnsec = 0;
	counter = 0;
	do {
		set();
		timespec_get(&sts, TIME_UTC);
		GB_NEON_unaligned();
		timespec_get(&ets, TIME_UTC);
		dsec += ets.tv_sec - sts.tv_sec;
		dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) {
			dsec--;
			dnsec += 1000000000ll;
		}
		dsec += dnsec / 1e9;
		dnsec = dnsec % (long long)1e9;
		verify();
		counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec;
	dsec = (tottime / counter) / 1e9;
	dnsec = (tottime / counter) % (long long)1e9;
	printf("GB_NEON_unaligned(average): %ld.%09llds\n", dsec, dnsec);
}
