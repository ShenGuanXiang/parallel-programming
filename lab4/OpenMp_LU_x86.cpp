/*x86平台下LU算法串行与各种OpenMP并行的对比*/

#include<windows.h>
#include<iostream>
#include<cstdlib>
#include<ctime>

#include<omp.h>
#include<nmmintrin.h> 
#include<immintrin.h> 
//随机数生成范围，设的比较小，防止初始化以及计算中经过多次行变换后数据溢出
#define MIN  -2
#define MAX   2
using namespace std;


const int NUM_THREADS = 4;//线程总数
int N = 1000;//矩阵行数兼列数
float** m = NULL;//输入矩阵

LARGE_INTEGER head, tail, freq;//计时器(总滴答数、每秒滴答数）
int counter;//重复测试计数器
double tottime;//每次重复测试的总时间

//构造矩阵
void set() {
	m = new float* [N];
	for (int i = 0; i < N; i++)
		m[i] = new float[N];
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < i; j++)
			m[i][j] = 0.0;
		m[i][i] = 1.0;
		for (int j = i + 1; j < N; j++) {
			m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
			while (m[i][j] == 0)
				m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
		}
	}
	//每个循环随机取两行，其中一行加上另一行
	for (int k = 0; k < N * N; k++) {
		int i1 = rand() % N, i2 = rand() % N;
		//先检验两行相加后是否有较大的值，再相加
		bool overflow = false;
		for (int j = 0; j < N; j++)
			if (m[i2][j] + m[i1][j] > 10000) { overflow = true; break; }
		if (!overflow)
			for (int j = 0; j < N; j++)
				m[i2][j] = m[i2][j] + m[i1][j];
	}
}

//回收空间
void destroy() {
	for (int i = 0; i < N; i++)
		delete[]m[i];
	delete[]m;
}

//串行算法
void Serial() {
	for (int k = 0; k < N; k++) {
		for (int j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			for (int j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

//水平划分
void omp_horizontal() {
	int i, j, k;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k)
	for (k = 0; k < N; k++)
	{
//#pragma omp for 
#pragma omp single
		{
			for (j = k + 1; j < N; j++)
				m[k][j] = m[k][j] / m[k][k];
			m[k][k] = 1.0;
		}
#pragma omp for
		for (i = k + 1; i < N; i++) {
			for (j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

//垂直划分
void omp_vertical() {
	int i, j, k;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k)
	for (k = 0; k < N; k++)
	{
#pragma omp for nowait	//消去部分垂直划分的情况下，除法多线程不需要等待
//#pragma omp for schedule(dynamic) nowait
//#pragma omp single
		for (j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		for (i = k + 1; i < N; i++) {
#pragma omp for
//#pragma omp for schedule(dynamic)
			for (j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
		m[k][k] = 1.0;  //最后置1
	}
}

//OpenMP+SSE
void omp_SSE() {
	int i, j, k;
	__m128 vkk, vkj, vik, vij;//不提前声明编译时会报错
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k,vkk,vkj,vik,vij)
	for (k = 0; k < N; k++)
	{
#pragma omp single//一定要括起来！
		{
			//SSE向量化除法操作
			vkk = _mm_set1_ps(m[k][k]);
			j = k + 1;
			for (; j + 4 <= N; j += 4) {
				vkj = _mm_loadu_ps(&m[k][j]);
				vkj = _mm_div_ps(vkj, vkk);
				_mm_storeu_ps(&m[k][j], vkj);
			}
			for (; j < N; j++) {
				m[k][j] = m[k][j] / m[k][k];
			}
			m[k][k] = 1.0;
		}
#pragma omp for
		for (i = k + 1; i < N; i++) {
			//每行的消去SSE向量化
			vik = _mm_set1_ps(m[i][k]);
			j = k + 1;
			for (; j + 4 <= N; j += 4) {
				vkj = _mm_loadu_ps(&m[k][j]);
				vij = _mm_loadu_ps(&m[i][j]);
				vij = _mm_sub_ps(vij, _mm_mul_ps(vik, vkj));
				_mm_storeu_ps(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
	}
}

//OpenMP+AVX
void omp_AVX() {
	int i, j, k;
	__m256 vkk, vkj, vik, vij;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k,vkk,vkj,vik,vij)
	for (k = 0; k < N; k++)
	{
#pragma omp single
		{
			//AVX向量化除法操作
			vkk = _mm256_set1_ps(m[k][k]);
			j = k + 1;
			for (; j + 8 <= N; j += 8) {
				vkj = _mm256_loadu_ps(&m[k][j]);
				vkj = _mm256_div_ps(vkj, vkk);
				_mm256_storeu_ps(&m[k][j], vkj);
			}
			for (; j < N; j++) {
				m[k][j] = m[k][j] / m[k][k];
			}
			m[k][k] = 1.0;
		}
#pragma omp for
		for (i = k + 1; i < N; i++) {
			//每行的消去AVX向量化
			vik = _mm256_set1_ps(m[i][k]);
			j = k + 1;
			for (; j + 8 <= N; j += 8) {
				vkj = _mm256_loadu_ps(&m[k][j]);
				vij = _mm256_loadu_ps(&m[i][j]);
				vij = _mm256_sub_ps(vij, _mm256_mul_ps(vik, vkj));
				_mm256_storeu_ps(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
	}
}

//OpenMP+AVX512
void omp_AVX512() {
	int i, j, k;
	__m512 vkk, vkj, vik, vij;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k,vkk,vkj,vik,vij)
	for (k = 0; k < N; k++)
	{
#pragma omp single
		{
			//AVX512向量化除法操作
			vkk = _mm512_set1_ps(m[k][k]);
			j = k + 1;
			for (; j + 16 <= N; j += 16) {
				vkj = _mm512_loadu_ps(&m[k][j]);
				vkj = _mm512_div_ps(vkj, vkk);
				_mm512_storeu_ps(&m[k][j], vkj);
			}
			for (; j < N; j++) {
				m[k][j] = m[k][j] / m[k][k];
			}
			m[k][k] = 1.0;
		}
#pragma omp for
		for (i = k + 1; i < N; i++) {
			//每行的消去AVX512向量化
			vik = _mm512_set1_ps(m[i][k]);
			j = k + 1;
			for (; j + 16 <= N; j += 16) {
				vkj = _mm512_loadu_ps(&m[k][j]);
				vij = _mm512_loadu_ps(&m[i][j]);
				vij = _mm512_sub_ps(vij, _mm512_mul_ps(vik, vkj));
				_mm512_storeu_ps(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
	}
}

//不同调度方式
void omp() {
	int i, j, k;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k)
	for (k = 0; k < N; k++) {
#pragma omp single
		{
			for (j = k + 1; j < N; j++)
				m[k][j] = m[k][j] / m[k][k];
			m[k][k] = 1.0;
		}    
#pragma omp for schedule(static,50)	
//#pragma omp for schedule(dynamic,50) 
//#pragma omp for schedule(guided,50)  
//#pragma omp for schedule(runtime) 
//#pragma omp for schedule(auto) 
		for (i = k + 1; i < N; i++) {
			for (j = k + 1; j < N; j++)
				m[i][j] -= m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
	}
}

//void print() {
//	for (int i = 0; i < N; i++) {
//		for (int j = 0; j < N; j++)
//			cout << m[i][j] << " ";
//		cout << endl;
//	}
//	cout << endl;
//}
//
////用来验证代码正确性，测量时间时无需调用
//void Verify() {
//	if (N > 10) return;
//	cout << "Serial: " << endl, set(), print(), Serial(), print(), destroy();
//	cout << "omp_horizontal: " << endl, set(), print(), omp_horizontal(), print(), destroy();
//	cout << "omp_vertical: " << endl, set(), print(), omp_vertical(), print(), destroy();
//	cout << "omp_SSE: " << endl, set(), print(), omp_SSE(), print(), destroy();
//	cout << "omp_AVX: " << endl, set(), print(), omp_AVX(), print(), destroy();
//	cout << "omp_AVX512: " << endl, set(), print(), omp_AVX512(), print(), destroy();
//	cout << "omp: " << endl, set(), print(), omp(), print(), destroy();
//}

//主函数对以上各种高斯消去法的函数分别计时，采用重复测试方法
int main() {
	srand(static_cast <unsigned> (time(0)));
	srand((unsigned)time(NULL));
	//Verify();
	QueryPerformanceFrequency(&freq);

	cout << "N=" << N << endl;
	cout << "NUM_THREADS=" << NUM_THREADS << endl;

	do {
		set(), QueryPerformanceCounter(&head);
		Serial();
		QueryPerformanceCounter(&tail), destroy(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 100 || counter < 4);
	cout << "Serial(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set(), QueryPerformanceCounter(&head);
		omp_horizontal();
		QueryPerformanceCounter(&tail), destroy(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 100 || counter < 4);
	cout << "omp_horizontal(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set(), QueryPerformanceCounter(&head);
		omp_vertical();
		QueryPerformanceCounter(&tail), destroy(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 100);
	cout << "omp_vertical(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set(), QueryPerformanceCounter(&head);
		omp_SSE();
		QueryPerformanceCounter(&tail), destroy(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 100 || counter < 4);
	cout << "omp_SSE(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set(), QueryPerformanceCounter(&head);
		omp_AVX();
		QueryPerformanceCounter(&tail), destroy(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 100 || counter < 4);
	cout << "omp_AVX(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set(), QueryPerformanceCounter(&head);
		omp_AVX512();
		QueryPerformanceCounter(&tail), destroy(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 100 || counter < 4);
	cout << "omp_AVX512(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set(), QueryPerformanceCounter(&head);
		omp();
		QueryPerformanceCounter(&tail), destroy(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 100 || counter < 4);
	cout << "omp(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;
}