/*arm平台下LU算法串行与各种OpenMP并行的对比*/

#include<time.h>
#include<iostream>
#include<cstdlib>
#include<ctime>

#include<omp.h>
#include<arm_neon.h>
//随机数生成范围，设的比较小，防止初始化以及计算中经过多次行变换后数据溢出
#define MIN  -2
#define MAX   2
using namespace std;


const int NUM_THREADS = 4;//线程总数
int N = 1000;//矩阵行数兼列数
float** m = NULL;//输入矩阵

struct timespec sts, ets;
time_t dsec;
long long dnsec;
int counter;//重复测试计数器
long long tottime;//每次重复测试的总时间

//构造矩阵
void set() {
	m = new float* [N];
	for (int i = 0; i < N; i++)
		m[i] = new float[N];
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < i; j++)
			m[i][j] = 0.0;
		m[i][i] = 1.0;
		for (int j = i + 1; j < N; j++) {
			m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
			while (m[i][j] == 0)
				m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
		}
	}
	//每个循环随机取两行，其中一行加上另一行
	for (int k = 0; k < N * N; k++) {
		int i1 = rand() % N, i2 = rand() % N;
		//先检验两行相加后是否有较大的值，再相加
		bool overflow = false;
		for (int j = 0; j < N; j++)
			if (m[i2][j] + m[i1][j] > 10000) { overflow = true; break; }
		if (!overflow)
			for (int j = 0; j < N; j++)
				m[i2][j] = m[i2][j] + m[i1][j];
	}
}

//回收空间
void destroy() {
	for (int i = 0; i < N; i++)
		delete[]m[i];
	delete[]m;
}

//串行算法
void Serial() {
	for (int k = 0; k < N; k++) {
		for (int j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
		for (int i = k + 1; i < N; i++) {
			for (int j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

//水平划分
void omp_horizontal() {
	int i, j, k;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k)
	for (k = 0; k < N; k++)
	{
		//#pragma omp for 
#pragma omp single
		{
			for (j = k + 1; j < N; j++)
				m[k][j] = m[k][j] / m[k][k];
			m[k][k] = 1.0;
		}
#pragma omp for
		for (i = k + 1; i < N; i++) {
			for (j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

//垂直划分
void omp_vertical() {
	int i, j, k;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k)
	for (k = 0; k < N; k++)
	{
#pragma omp for nowait	//消去部分垂直划分的情况下，除法多线程不需要等待
		for (j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		for (i = k + 1; i < N; i++) {
#pragma omp for
			for (j = k + 1; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
		m[k][k] = 1.0;  //最后置1
	}
}

//OpenMP+NEON
void omp_NEON() {
	int i, j, k;
	float32x4_t vkk, vkj, vik, vij;//不提前声明编译时会报错
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k,vkk,vkj,vik,vij)
	for (k = 0; k < N; k++)
	{
#pragma omp single//一定要括起来！
		{
			//NEON向量化除法操作
			vkk = vld1q_dup_f32(&m[k][k]);
			j = k + 1;
			for (; j + 4 <= N; j += 4) {
				vkj = vld1q_f32(&m[k][j]);
				vkj = vdivq_f32(vkj, vkk);
				vst1q_f32(&m[k][j], vkj);
			}
			for (; j < N; j++) {
				m[k][j] = m[k][j] / m[k][k];
			}
			m[k][k] = 1.0;
		}
#pragma omp for
		for (i = k + 1; i < N; i++) {
			//每行的消去NEON向量化
			vik = vld1q_dup_f32(&m[i][k]);
			j = k + 1;
			for (; j + 4 <= N; j += 4) {
				vkj = vld1q_f32(&m[k][j]);
				vij = vld1q_f32(&m[i][j]);
				vij = vsubq_f32(vij, vmulq_f32(vik, vkj));
				vst1q_f32(&m[i][j], vij);
			}
			for (; j < N; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}

//自动simd
void omp_simd() {
	int i, j, k;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k)
	for (k = 0; k < N; k++) {
#pragma omp for simd
		for (j = k + 1; j < N; j++)
			m[k][j] = m[k][j] / m[k][k];
		m[k][k] = 1.0;
#pragma omp for simd     
		for (i = k + 1; i < N; i++) {
			for (j = k + 1; j < N; j++)
				m[i][j] -= m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
	}
}

//动态划分
void omp_dynamic() {
	int i, j, k;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k)
	for (k = 0; k < N; k++) {
#pragma omp single
		{
			for (j = k + 1; j < N; j++)
				m[k][j] = m[k][j] / m[k][k];
			m[k][k] = 1.0;
		}
#pragma omp for schedule(dynamic,1)
		for (i = k + 1; i < N; i++) {
			for (j = k + 1; j < N; j++)
				m[i][j] -= m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
	}
}

//guided调度
void omp_guided() {
	int i, j, k;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k)
	for (k = 0; k < N; k++) {
#pragma omp single
		{
			for (j = k + 1; j < N; j++)
				m[k][j] = m[k][j] / m[k][k];
			m[k][k] = 1.0;
		}
#pragma omp for schedule(guided)
		for (i = k + 1; i < N; i++) {
			for (j = k + 1; j < N; j++)
				m[i][j] -= m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
	}
}

//runtime调度
void omp_runtime() {
	int i, j, k;
#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k)
	for (k = 0; k < N; k++) {
#pragma omp single
		{
			for (j = k + 1; j < N; j++)
				m[k][j] = m[k][j] / m[k][k];
			m[k][k] = 1.0;
		}
#pragma omp for schedule(runtime)
		for (i = k + 1; i < N; i++) {
			for (j = k + 1; j < N; j++)
				m[i][j] -= m[i][k] * m[k][j];
			m[i][k] = 0.0;
		}
	}
}

//void print() {
//	for (int i = 0; i < N; i++) {
//		for (int j = 0; j < N; j++)
//			cout << m[i][j] << " ";
//		cout << endl;
//	}
//	cout << endl;
//}
//
////用来验证代码正确性，测量时间时无需调用
//void Verify() {
//	if (N > 10) return;
//	cout << "Serial: " << endl, set(), print(), Serial(), print(), destroy();
//	cout << "omp_horizontal: " << endl, set(), print(), omp_horizontal(), print(), destroy();
//	cout << "omp_vertical: " << endl, set(), print(), omp_vertical(), print(), destroy();
//	cout << "omp_NEON: " << endl, set(), print(), omp_NEON(), print(), destroy();
//	cout << "omp_simd: " << endl, set(), print(), omp_simd(), print(), destroy();
//	cout << "omp_dynamic: " << endl, set(), print(), omp_dynamic(), print(), destroy();
//	cout << "omp_guided: " << endl, set(), print(), omp_guided(), print(), destroy();
//	cout << "omp_runtime: " << endl, set(), print(), omp_runtime(), print(), destroy();
//}

//主函数对以上各种高斯消去法的函数分别计时，采用重复测试方法
int main() {
	srand(static_cast <unsigned> (time(0)));
	srand((unsigned)time(NULL));
	//Verify();

	cout << "N=" << N << endl;
	cout << "NUM_THREADS=" << NUM_THREADS << endl;

	do {
		set(); timespec_get(&sts, TIME_UTC);
		Serial();
		timespec_get(&ets, TIME_UTC); dsec += ets.tv_sec - sts.tv_sec; dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) dsec--, dnsec += 1000000000ll;
		dsec += dnsec / 1e9; dnsec = dnsec % (long long)1e9;
		destroy(); counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec; dsec = (tottime / counter) / 1e9; dnsec = (tottime / counter) % (long long)1e9;
	printf("Serial(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0; dnsec = 0; counter = 0;

	do {
		set(); timespec_get(&sts, TIME_UTC);
		omp_horizontal();
		timespec_get(&ets, TIME_UTC); dsec += ets.tv_sec - sts.tv_sec; dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) dsec--, dnsec += 1000000000ll;
		dsec += dnsec / 1e9; dnsec = dnsec % (long long)1e9;
		destroy(); counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec; dsec = (tottime / counter) / 1e9; dnsec = (tottime / counter) % (long long)1e9;
	printf("omp_horizontal(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0; dnsec = 0; counter = 0;

	do {
		set(); timespec_get(&sts, TIME_UTC);
		omp_vertical();
		timespec_get(&ets, TIME_UTC); dsec += ets.tv_sec - sts.tv_sec; dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) dsec--, dnsec += 1000000000ll;
		dsec += dnsec / 1e9; dnsec = dnsec % (long long)1e9;
		destroy(); counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec; dsec = (tottime / counter) / 1e9; dnsec = (tottime / counter) % (long long)1e9;
	printf("omp_vertical(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0; dnsec = 0; counter = 0;

	do {
		set(); timespec_get(&sts, TIME_UTC);
		omp_NEON();
		timespec_get(&ets, TIME_UTC); dsec += ets.tv_sec - sts.tv_sec; dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) dsec--, dnsec += 1000000000ll;
		dsec += dnsec / 1e9; dnsec = dnsec % (long long)1e9;
		destroy(); counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec; dsec = (tottime / counter) / 1e9; dnsec = (tottime / counter) % (long long)1e9;
	printf("omp_NEON(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0; dnsec = 0; counter = 0;

	do {
		set(); timespec_get(&sts, TIME_UTC);
		omp_simd();
		timespec_get(&ets, TIME_UTC); dsec += ets.tv_sec - sts.tv_sec; dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) dsec--, dnsec += 1000000000ll;
		dsec += dnsec / 1e9; dnsec = dnsec % (long long)1e9;
		destroy(); counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec; dsec = (tottime / counter) / 1e9; dnsec = (tottime / counter) % (long long)1e9;
	printf("omp_simd(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0; dnsec = 0; counter = 0;

	do {
		set(); timespec_get(&sts, TIME_UTC);
		omp_dynamic();
		timespec_get(&ets, TIME_UTC); dsec += ets.tv_sec - sts.tv_sec; dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) dsec--, dnsec += 1000000000ll;
		dsec += dnsec / 1e9; dnsec = dnsec % (long long)1e9;
		destroy(); counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec; dsec = (tottime / counter) / 1e9; dnsec = (tottime / counter) % (long long)1e9;
	printf("omp_dynamic(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0; dnsec = 0; counter = 0;

	do {
		set(); timespec_get(&sts, TIME_UTC);
		omp_guided();
		timespec_get(&ets, TIME_UTC); dsec += ets.tv_sec - sts.tv_sec; dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) dsec--, dnsec += 1000000000ll;
		dsec += dnsec / 1e9; dnsec = dnsec % (long long)1e9;
		destroy(); counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec; dsec = (tottime / counter) / 1e9; dnsec = (tottime / counter) % (long long)1e9;
	printf("omp_guided(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0; dnsec = 0; counter = 0;

	do {
		set(); timespec_get(&sts, TIME_UTC);
		omp_runtime();
		timespec_get(&ets, TIME_UTC); dsec += ets.tv_sec - sts.tv_sec; dnsec += ets.tv_nsec - sts.tv_nsec;
		if (dnsec < 0) dsec--, dnsec += 1000000000ll;
		dsec += dnsec / 1e9; dnsec = dnsec % (long long)1e9;
		destroy(); counter++;
	} while (1e9 * dsec + dnsec < 2.5e7);
	tottime = 1e9 * dsec + dnsec; dsec = (tottime / counter) / 1e9; dnsec = (tottime / counter) % (long long)1e9;
	printf("omp_runtime(average): %ld.%09llds\n", dsec, dnsec);
	dsec = 0; dnsec = 0; counter = 0;
}