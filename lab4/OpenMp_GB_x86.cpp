/*x86平台下特殊高斯消去算法串行与OpenMp并行的对比*/

#include<windows.h>
#include<iostream>
#include<cstdlib>
#include<ctime>
#include<cstring>
#include<fstream>
#include<sstream>

#include<omp.h>
#include<nmmintrin.h> 
#include<immintrin.h> 
using namespace std;

const int NUM_THREADS = 5;//线程总数
const int N = 1011;//矩阵列数
const int m = 263;//被消元行总行数
string elimination_txt = "4_1011_539_263//1.txt", deleted_row_txt = "4_1011_539_263//2.txt", result_txt = "4_1011_539_263//3.txt";//消元子、被消元行、消元结果文件路径
const int len = (N - 1) / 32 + 1;//Mybitset中bits数组元素个数(N/32向上取整)

LARGE_INTEGER head, tail, freq;//计时器(总滴答数、每秒滴答数）
int counter;//重复测试计数器
double tottime;//每次重复测试的总时间

//bit位从右（高地址）向左（低地址）开始从0递增
class Mybitset {
public:
	int32_t bits[len];
	Mybitset() {
		reset();
	}
	//置位
	void set(int pos, bool value) {
		if (value == 1)
			bits[len - 1 - pos / 32] |= value << (pos % 32);
		else
			bits[len - 1 - pos / 32] &= (~(value << (pos % 32)));
	}
	//获取一位的值
	bool get(int pos) {
		return (bits[len - 1 - pos / 32]) & (1 << (pos % 32));
	}
	//首项（从左向右首个为1的bit位）
	int lp() {
		if (none()) return -1;
		int i = -1;
		while (i < len - 1 && bits[++i] == 0);
		int j = 31;
		for (; j >= 0 && ((bits[i] >> j) & 1) == 0; j--);
		return (len - 1 - i) * 32 + j;
	}
	//判断是否全0
	bool none() {
		for (int i = 0; i < len; i++)
			if (bits[i] != 0)
				return false;
		return true;
	}
	//全部置0
	void reset() {
		for (int i = 0; i < len; i++)
			bits[i] = 0;
	}

	//判断是否相等
	bool equal(Mybitset another) {
		for (int i = 0; i < len; i++)
			if (bits[i] != another.bits[i])
				return false;
		return true;
	}
}*R[N], E[m];//R:所有消元子构成的集合，R[i]:首项为i的消元子; E:所有被消元行构成的数组 


//转义为01串
void set() {
	string line;
	for (int i = 0; i < m; i++) E[i].reset();
	for (int i = 0; i < N; i++)
		if (R[i] != NULL) {
			if (!R[i]->none())
				delete R[i];
			R[i] = NULL;
		}
	//读入消元子
	ifstream  input_elimination(elimination_txt);
	while (getline(input_elimination, line)) {
		stringstream ss;
		ss << line;
		int lp;
		ss >> lp;
		if (R[lp] == NULL) R[lp] = new Mybitset();
		R[lp]->set(lp, 1);
		if (!ss.eof()) {
			int pos;
			while (ss >> pos)
				R[lp]->set(pos, 1);
		}
	}
	input_elimination.close();
	//读入被消元行
	ifstream input_deleted_row(deleted_row_txt);
	for (int i = 0; i < m; i++) {
		getline(input_deleted_row, line);
		stringstream ss;
		ss << line;
		int pos;
		while (ss >> pos)
			E[i].set(pos, 1);
	}
	input_deleted_row.close();
}

////特殊高斯消去法串行实现
//void Serial() {
//	for (int k = 0; k < m; k++) {
//		while (!E[k].none()) {
//			int E_k_lp = E[k].lp();//取被消元行E[k]首项
//			bool have_Rlp = R[E_k_lp]!=NULL;//是否有相应消元子
//			if (have_Rlp)
//				//串行异或
//				for (int i = 0; i < len; i++)
//					E[k].bits[i] ^= R[E_k_lp]->bits[i];
//			else {
//				R[E_k_lp] = &E[k];//被消元行升格为消元子
//				break;
//			}
//		}
//	}
//}


//特殊高斯消去法串行实现, 外层循环改为遍历消元子，这种形式更易于多线程并行，总体时间复杂度仍为O(mN²)，且实测更快
void Serial() {
	bool f[m] = { false };//对应的被消元行是否被升格为消元子
	for (int k = N - 1; k >= 0; k--) {
		for (int i = 0; i < m; i++) {
			if (E[i].get(k) && !f[i]) {
				//没有对应消元子，则被消元行升格为消元子
				if (R[k] == NULL) {
					R[k] = &E[i];
					f[i] = true;
				}
				//串行异或
				else {
					for (int j = 0; j < len; j++)
						E[i].bits[j] ^= R[k]->bits[j];
				}
			}
		}
	}
}

//每轮消元水平划分，OpenMP多线程执行
void omp() {
	bool f[m] = { false };//对应的被消元行是否被升格为消元子
	int i, j, k, t;
	#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k,t)
	for (k = N - 1; k >= 0; k--) {
		#pragma omp single
		if (R[k] == NULL) {
			for (t = 0; t < m; t++) {
				if (E[t].get(k) && !f[t]) {
					R[k] = &E[t];
					f[t] = true;
					break;
				}
			}
		}
		if (R[k] != NULL) {
			#pragma omp for
			for (i = 0; i < m; i++) {
				if (E[i].get(k) && !f[i])
					//串行异或
					for (j = 0; j < len; j++)
						E[i].bits[j] ^= R[k]->bits[j];
			}
		}
	}
}

//OpenMP+SSE
void omp_SSE() {
	bool f[m] = { false };//对应的被消元行是否被升格为消元子
	int i, j, k, t;
	#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k,t)
	for (k = N - 1; k >= 0; k--) {
		#pragma omp single
		if (R[k] == NULL) {
			for (t = 0; t < m; t++) {
				if (E[t].get(k) && !f[t]) {
					R[k] = &E[t];
					f[t] = true;
					break;
				}
			}
		}
		if (R[k] != NULL) {
			#pragma omp for
			for (i = 0; i < m; i++) {
				if (E[i].get(k) && !f[i]) {
					//SSE并行异或
					for (j = 0; j + 4 <= len; j += 4) {
						__m128i va = _mm_loadu_si128((__m128i*) & E[i].bits[j]);
						__m128i vb = _mm_loadu_si128((__m128i*) & R[k]->bits[j]);
						va = _mm_xor_si128(va, vb);
						_mm_storeu_si128((__m128i*) & E[i].bits[j], va);
					}
					for (; j < len; j++)
						E[i].bits[j] ^= R[k]->bits[j];
				}
			}
		}
	}
}

//OpenMP+AVX
void omp_AVX() {
	bool f[m] = { false };//对应的被消元行是否被升格为消元子
	int i, j, k, t;
	#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k,t)
	for (k = N - 1; k >= 0; k--) {
		#pragma omp single
		if (R[k] == NULL) {
			for (t = 0; t < m; t++) {
				if (E[t].get(k) && !f[t]) {
					R[k] = &E[t];
					f[t] = true;
					break;
				}
			}
		}
		if (R[k] != NULL) {
			#pragma omp for
			for (i = 0; i < m; i++) {
				if (E[i].get(k) && !f[i]) {
					//AVX并行异或
					for (j = 0; j + 8 <= len; j += 8) {
						__m256i va = _mm256_loadu_si256((__m256i*) & E[i].bits[j]);
						__m256i vb = _mm256_loadu_si256((__m256i*) & R[k]->bits[j]);
						va = _mm256_xor_si256(va, vb);
						_mm256_storeu_si256((__m256i*) & E[i].bits[j], va);
					}
					for (; j < len; j++)
						E[i].bits[j] ^= R[k]->bits[j];
				}
			}
		}
	}
}

//OpenMP+AVX512
void omp_AVX512() {
	bool f[m] = { false };//对应的被消元行是否被升格为消元子
	int i, j, k, t;
	#pragma omp parallel num_threads(NUM_THREADS), private(i,j,k,t)
	for (k = N - 1; k >= 0; k--) {
		#pragma omp single
		if (R[k] == NULL) {
			for (t = 0; t < m; t++) {
				if (E[t].get(k) && !f[t]) {
					R[k] = &E[t];
					f[t] = true;
					break;
				}
			}
		}
		if (R[k] != NULL) {
			#pragma omp for
			for (i = 0; i < m; i++) {
				if (E[i].get(k) && !f[i]) {
					//AVX512并行异或
					for (j = 0; j + 16 <= len; j += 16) {
						__m512i va = _mm512_loadu_si512(&E[i].bits[j]);
						__m512i vb = _mm512_loadu_si512(&R[k]->bits[j]);
						va = _mm512_xor_si512(va, vb);
						_mm512_storeu_si512(&E[i].bits[j], va);
					}
					for (; j < len; j++)
						E[i].bits[j] ^= R[k]->bits[j];
				}
			}
		}
	}
}

//对比自己的算法的消元结果和正确的结果，验证算法正确性
void verify() {
	Mybitset result[m];//正确的消元结果，用于检验
	ifstream input_result_txt(result_txt);
	string line;
	for (int i = 0; i < m; i++) {
		stringstream ss;
		getline(input_result_txt, line);
		ss << line;
		if (!ss.eof()) {
			int pos;
			while (ss >> pos)
				result[i].set(pos, 1);
		}
	}
	for (int i = 0; i < m; i++) {
		/*for (int t = N - 1; t >= 0; t--)
			if (E[i].get(t)) cout << t << " ";
		cout << endl;*/
		if (!E[i].equal(result[i])) {
			cout << "error" << endl;
			exit(2);
		}
	}
	input_result_txt.close();
}

//主函数对以上各种特殊高斯消去法的函数分别计时，采用重复测试方法
int main() {
	QueryPerformanceFrequency(&freq);

	do {
		set(), QueryPerformanceCounter(&head);
		Serial();
		QueryPerformanceCounter(&tail), verify(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 4);
	cout << "Serial(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set(), QueryPerformanceCounter(&head);
		omp();
		QueryPerformanceCounter(&tail), verify(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 4);
	cout << "omp(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set(), QueryPerformanceCounter(&head);
		omp_SSE();
		QueryPerformanceCounter(&tail), verify(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 4);
	cout << "omp_SSE(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set(), QueryPerformanceCounter(&head);
		omp_AVX();
		QueryPerformanceCounter(&tail), verify(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 4);
	cout << "omp_AVX(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	do {
		set(), QueryPerformanceCounter(&head);
		omp_AVX512();
		QueryPerformanceCounter(&tail), verify(), counter++;
		tottime += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
	} while (tottime < 50 || counter < 4);
	cout << "omp_AVX512(average): " << tottime / counter << "ms" << endl;
	counter = 0; tottime = 0;

	//回收空间
	for (int i = 0; i < m; i++) E[i].reset();
	for (int i = 0; i < N; i++)
		if (R[i] != NULL) {
			if (!R[i]->none())
				delete R[i];
			R[i] = NULL;
		}
}