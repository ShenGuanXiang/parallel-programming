
#include<iostream>
#include<cstdlib>
#include<ctime>
#include<cstring>
#include<fstream>
#include<sstream>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

using namespace std;

const int N = 1011;//矩阵列数
const int m = 263;//被消元行总行数
string elimination_txt = "4_1011_539_263//1.txt", deleted_row_txt = "4_1011_539_263//2.txt", result_txt = "4_1011_539_263//3.txt";//消元子、被消元行、消元结果文件路径
const int len = (N - 1) / 32 + 1;//Mybitset中bits数组元素个数(N/32向上取整)


size_t blocks_per_grid = 512;
size_t threads_per_block = 1024;
cudaError_t err;

//bit位从右（高地址）向左（低地址）开始从0递增
class Mybitset {
public:
	int32_t bits[len];
	Mybitset() {
		reset();
	}
	//置位
	void set(int pos, bool value) {
		if (value == 1)
			bits[len - 1 - pos / 32] |= value << (pos % 32);
		else
			bits[len - 1 - pos / 32] &= (~(value << (pos % 32)));
	}
	//获取一位的值(在GPU/CPU上)
	__device__ bool get_in_device(int pos) {
		return (bits[len - 1 - pos / 32]) & (1 << (pos % 32));
	}
	bool get(int pos) {
		return (bits[len - 1 - pos / 32]) & (1 << (pos % 32));
	}
	//判断是否全0
	bool none() {
		for (int i = 0; i < len; i++)
			if (bits[i] != 0)
				return false;
		return true;
	}
	//全部置0
	void reset() {
		for (int i = 0; i < len; i++)
			bits[i] = 0;
	}

	//判断是否相等
	bool equal(Mybitset another) {
		for (int i = 0; i < len; i++)
			if (bits[i] != another.bits[i])
				return false;
		return true;
	}
}*R[N], E[m];//R:所有消元子构成的集合，R[i]:首项为i的消元子; E:所有被消元行构成的数组


//转义为01串
void set() {
	string line;
	for (int i = 0; i < m; i++) E[i].reset();
	for (int i = 0; i < N; i++)
		if (R[i] != NULL) {
			if (!R[i]->none())
				delete R[i];
			R[i] = NULL;
		}
	//读入消元子
	ifstream  input_elimination(elimination_txt);
	while (getline(input_elimination, line)) {
		stringstream ss;
		ss << line;
		int lp;
		ss >> lp;
		if (R[lp] == NULL) R[lp] = new Mybitset();
		R[lp]->set(lp, 1);
		if (!ss.eof()) {
			int pos;
			while (ss >> pos)
				R[lp]->set(pos, 1);
		}
	}
	input_elimination.close();
	//读入被消元行
	ifstream input_deleted_row(deleted_row_txt);
	for (int i = 0; i < m; i++) {
		getline(input_deleted_row, line);
		stringstream ss;
		ss << line;
		int pos;
		while (ss >> pos)
			E[i].set(pos, 1);
	}
	input_deleted_row.close();
}

__global__ void whether_to_eliminate_kernel(Mybitset E[], bool* f, int k, bool* eliminate) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = gridDim.x * blockDim.x;
	for (int i = idx; i < m; i += stride)
	{
		eliminate[i] = E[i].get_in_device(k) && !f[i];
	}
}

__global__ void eliminate_kernel(Mybitset E[], Mybitset Rk, bool* eliminate, int k) {
	for (int i = blockIdx.x; i < m; i += gridDim.x) {
		if (eliminate[i]) {
			for (int j = threadIdx.x; j < len; j += blockDim.x)
				E[i].bits[j] ^= Rk.bits[j];
			__syncthreads();//块内同步
		}
	}
}


void GB_cuda(Mybitset E[]) {
	bool* f;//对应的被消元行是否被升格为消元子
	cudaMallocManaged(&f, m * sizeof(bool));
	for (int i = 0; i < m; i++)
		f[i] = false;
	bool* eliminate;
	cudaMallocManaged(&eliminate, m * sizeof(bool));
	for (int k = N - 1; k >= 0; k--) {
		if (R[k] == NULL) {
			for (int i = 0; i < m; i++) {
				if (E[i].get(k) && !f[i]) {
					R[k] = &E[i];
					f[i] = true;
					break;
				}
			}
		}
		if (R[k] != NULL) {
			whether_to_eliminate_kernel <<<blocks_per_grid, threads_per_block>>> (E, f, k, eliminate);
			cudaDeviceSynchronize();
			err = cudaGetLastError();
			if (err != cudaSuccess) {
				printf("whether_to_eliminate_kernel failed, %s\n", cudaGetErrorString(err));
			}
			eliminate_kernel <<<blocks_per_grid, threads_per_block >>> (E, *R[k], eliminate, k);
			cudaDeviceSynchronize();
			err = cudaGetLastError();
			if (err != cudaSuccess) {
				printf("elimination_kernel failed, %s\n", cudaGetErrorString(err));
			}
		}
	}
	cudaFree(f);
	cudaFree(eliminate);
}

//对比自己的算法的消元结果和正确的结果，验证算法正确性
void verify() {
	Mybitset result[m];//正确的消元结果，用于检验
	ifstream input_result_txt(result_txt);
	string line;
	for (int i = 0; i < m; i++) {
		stringstream ss;
		getline(input_result_txt, line);
		ss << line;
		if (!ss.eof()) {
			int pos;
			while (ss >> pos)
				result[i].set(pos, 1);
		}
	}
	for (int i = 0; i < m; i++) {
		/*for (int t = N - 1; t >= 0; t--)
			if (E[i].get(t)) cout << t << " ";
		cout << endl;*/
		if (!E[i].equal(result[i])) {
			cout << "error" << endl;
			exit(2);
		}
	}
	input_result_txt.close();
}

int main() {
	set();

	Mybitset * E_;//用于GPU
	err = cudaMalloc(&E_, m * sizeof(Mybitset));
	if (err != cudaSuccess)
		printf("cudaMalloc failed!\n");

	//将数据传输至 GPU 端
	err = cudaMemcpy(E_, E, m * sizeof(Mybitset), cudaMemcpyHostToDevice);
	if (err != cudaSuccess) {
		printf("cudaMemcpyHostToDevice failed!\n");
	}
	
	cudaEvent_t start, end;//计时器
	float elapsedTime = 0.0;
	cudaEventCreate(&start);
	cudaEventCreate(&end);
	cudaEventRecord(start, 0);//开始计时
	GB_cuda(E_);
	cudaEventRecord(end, 0);
	cudaEventSynchronize(end);//停止计时
	cudaEventElapsedTime(&elapsedTime, start, end);
	printf("GB_cuda:%f ms\n", elapsedTime);

	//销毁计时器
	cudaEventDestroy(start);
	cudaEventDestroy(end);

	//将数据传回 CPU 端
	err = cudaMemcpy(E, E_, m * sizeof(Mybitset), cudaMemcpyDeviceToHost);
	if (err != cudaSuccess)
		printf("cudaMemcpyDeviceToHost failed!\n");

	verify();
	cudaFree(E_);
}
