#include<iostream>
#include<cstdlib>
#include<ctime>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"


//随机数生成范围，设的比较小，防止初始化以及计算中经过多次行变换后数据溢出
#define MIN  -2
#define MAX   2

using namespace std;

const int N = 2048;//矩阵行数兼列数
float m[N][N] = { 0 };//输入矩阵

size_t blocks_per_grid = 256;
size_t threads_per_block = 1024;
cudaError_t err;

//构造矩阵
void init() {
	//随机数初始化一个上三角矩阵
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < i; j++)
			m[i][j] = 0.0;
		m[i][i] = 1.0;
		for (int j = i + 1; j < N; j++) {
			m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
			while (m[i][j] == 0)
				m[i][j] = MIN + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (MAX - MIN)));
		}
	}
	//每个循环随机取两行，其中一行加上另一行
	for (int k = 0; k < N * N; k++) {
		int i1 = rand() % N, i2 = rand() % N;
		//先检验两行相加后是否有较大的值，再相加
		bool overflow = false;
		for (int j = 0; j < N; j++)
			if (m[i2][j] + m[i1][j] > 10000) { overflow = true; break; }
		if (!overflow)
			for (int j = 0; j < N; j++)
				m[i2][j] = m[i2][j] + m[i1][j];
	}
}

__global__ void division_kernel(float* data, int k) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = gridDim.x * blockDim.x;
	for (int i = k + 1 + idx; i < N; i += stride)
	{
		data[k * N + i] /= data[k * N + k];
	}
}

__global__ void eliminate_kernel(float* data, int k) {
	for (int i = k + 1 + blockIdx.x; i < N; i += gridDim.x) {
		for (int j = k + 1 + threadIdx.x; j < N; j += blockDim.x) {
			data[i * N + j] -= data[i * N + k] * data[k * N + j];
		}
		__syncthreads();//块内同步
		if (threadIdx.x == 0) {
			data[i * N + k] = 0;
		}
	}
}

void LU_cuda(float* data) {
	for (int k = 0; k < N; k++) {
		division_kernel<<<blocks_per_grid, threads_per_block>>>(data, k);
		cudaDeviceSynchronize();//各线程同步
		data[k * N + k] = 1.0;
		err = cudaGetLastError();
		if (err != cudaSuccess) {
			printf("division_kernel failed, %s\n", cudaGetErrorString(err));
		}
		eliminate_kernel<<<blocks_per_grid, threads_per_block>>>(data, k);
		cudaDeviceSynchronize();
		err = cudaGetLastError();
		if (err != cudaSuccess) {
			printf("elimination_kernel failed, %s\n", cudaGetErrorString(err));
		}
	}
}

int main() {
	init();

	if (N < 10) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++)
				cout << m[i][j] << " ";
			cout << endl;
		}
	}

	float* a;//用于CPU和GPU之间的数据传递
	err = cudaMallocManaged(&a, N * N * sizeof(float));
	if (err != cudaSuccess)
		printf("cudaMallocManaged failed!\n");
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++)
			a[i * N + j] = m[i][j];
	}

	cudaEvent_t start, end;//计时器
	float elapsedTime = 0.0;
	cudaEventCreate(&start);
	cudaEventCreate(&end);
	cudaEventRecord(start, 0);//开始计时
	LU_cuda(a);
	cudaEventRecord(end, 0);
	cudaEventSynchronize(end);//停止计时
	cudaEventElapsedTime(&elapsedTime, start, end);
	printf("LU_cuda:%f ms\n", elapsedTime);

	//销毁计时器
	cudaEventDestroy(start);
	cudaEventDestroy(end);
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++)
			m[i][j] = a[i * N + j];
	}

	if (N < 10) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++)
				cout << m[i][j] << " ";
			cout << endl;
		}
	}
	cudaFree(a);
}
