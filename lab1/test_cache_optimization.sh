# test.sh
# !/bin/sh
pssh -h $PBS_NODEFILE -i "if [ ! -d \"/home/s2012805/Lab1/test\" ];then mkdir -p \"/home/s2012805/Lab1/test\"; fi" 1>&2  
pscp -h $PBS_NODEFILE /home/s2012805/Lab1/cache_optimization /home/s2012805/Lab1/test 1>&2
/home/s2012805/Lab1/test/cache_optimization
cd /home/s2012805/Lab1
perf record -e cache-misses,cache-references,L1-dcache-loads,L1-dcache-load-misses,L1-dcache-stores,L1-icache-loads,L1-icache-load-misses -g /home/s2012805/Lab1/test/cache_optimization 
