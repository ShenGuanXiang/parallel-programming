#include <iostream>
#include <random>
#include <windows.h>
#include <stdlib.h>

using namespace std;

const int N = 8;//问题规模，可自行修改
long long counter;//重复测试计数器
double a[N], b[N][N];
double res[2][N];//res[0]存储逐列运算的结果，res[1]存储逐行运算的结果
LARGE_INTEGER head, tail, freq;//计时器(总滴答数、每秒滴答数）
double col_time, row_time;//逐列、逐行运算的总时间

//伪随机数初始化a、b这两个double数组
void random_init() {
    constexpr int MIN = 0x80000001;
    constexpr int MAX = 0x7FFFFFFF;//在最小负数和最大正数之间随机生成双精度浮点数
    std::random_device rd;
    std::default_random_engine eng(rd());
    std::uniform_real_distribution<double> distr(MIN, MAX);
    for (int i = 0; i < N; i++) {
        a[i] = distr(eng);
        for (int j = 0; j < N; j++)
            b[i][j] = distr(eng);
    }
}

//逐列计算
void col_major() {
    for (int j = 0; j < N; j++)
        for (int i = 0; i < N; i++)
            res[0][j] += a[i] * b[i][j];
}

//逐行计算
void row_major() {
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            res[1][j] += a[i] * b[i][j];
}


int main()
{
    QueryPerformanceFrequency(&freq);
    do {
        random_init();
        QueryPerformanceCounter(&head);
        col_major();
        QueryPerformanceCounter(&tail);
        col_time += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
        counter++;
    } while (col_time < 50);//让col_major函数总的执行时间不小于50ms
    cout << "col_major(average): " << col_time / counter << "ms" <<"  重复次数:"<< counter << endl;
    counter = 0;
    QueryPerformanceCounter(&head);
    do {
        random_init();
        QueryPerformanceCounter(&head);
        row_major();
        QueryPerformanceCounter(&tail);
        row_time += (tail.QuadPart - head.QuadPart) * 1000.0 / freq.QuadPart;
        counter++;
    } while (row_time < 50);
    cout << "row_major(average): " << row_time / counter << "ms" <<"  重复次数:"<< counter << endl;
    return EXIT_SUCCESS;
}


