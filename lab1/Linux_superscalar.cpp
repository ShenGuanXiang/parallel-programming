#include <iostream>
#include <string.h>
#include <sys/time.h>
#include <ctime>
#define MIN -100
#define MAX 100//数据范围[MIN,MAX]
using namespace std;
const int N = 32;//问题规模按2的幂次递增，可自行修改
typedef int T;//实现浮点数和整数的对比
T a[N], b[N];//b[N]用来备份，当某一算法原地计算改变a[N]的元素值后，通过b[N]恢复初值，为下一个算法做准备;
T sum;//求出来的数组元素的和
struct timeval head, tail;//计时器
long long counter;//重复测试计数器
double timeval;//记录时间数值

////生成[MIN,MAX]之间随机浮点数
//void random_init(int N) {
//    for (int i = 0; i < N; i++)
//        b[i] = a[i] = MIN + (double)(rand()) / RAND_MAX * (MAX - MIN);
//}

//生成[MIN,MAX]之间随机整数
void random_init() {
    srand((unsigned)(time(0)));
    for (int i = 0; i < N; i++)
        b[i] = a[i] = (rand() % (MAX - MIN + 1)) + MIN;
}

//链式
void chain() {
    for (int i = 0; i < N; i++)
        sum += a[i];
}

//链式循环展开，将八个循环步展开到一个循环步
void chain_unroll() {
    for (int i = 0; i < N; i += 8) {
        sum += a[i];
        sum += a[i + 1];
        sum += a[i + 2];
        sum += a[i + 3];
        sum += a[i + 4];
        sum += a[i + 5];
        sum += a[i + 6];
        sum += a[i + 7];
    }
}

//双链路式循环展开
void doublechain_unroll() {
    T sum1 = 0, sum2 = 0;
    for (int i = 0; i < N; i += 16) {
        sum1 += a[i];
        sum2 += a[i + 1];
        sum1 += a[i + 2];
        sum2 += a[i + 3];
        sum1 += a[i + 4];
        sum2 += a[i + 5];
        sum1 += a[i + 6];
        sum2 += a[i + 7];
        sum1 += a[i + 8];
        sum2 += a[i + 9];
        sum1 += a[i + 10];
        sum2 += a[i + 11];
        sum1 += a[i + 12];
        sum2 += a[i + 13];
        sum1 += a[i + 14];
        sum2 += a[i + 15];
    }
    sum = sum1 + sum2;
}

//递归式循环展开
void recursion_unroll(int n) {
    if (n == 1) {
        sum = a[0];
        return;
    }
    for (int i = 0; i < n / 2; i += 8) {
        a[i] += a[n - i - 1];
        a[i + 1] += a[n - i - 2];
        a[i + 2] = a[n - i - 3];
        a[i + 3] = a[n - i - 4];
        a[i + 4] = a[n - i - 5];
        a[i + 5] = a[n - i - 6];
        a[i + 6] = a[n - i - 7];
        a[i + 7] = a[n - i - 8];
    }
    recursion_unroll(n / 2);
}

//二重循环（循环展开）
void circulation_unroll() {
    for (int m = N; m > 15; m /= 2)
        for (int i = 0; i < m / 2; i += 8) {
            a[i] = a[2 * i] + a[2 * i + 1];
            a[i + 1] = a[2 * (i + 1)] + a[2 * (i + 1) + 1];
            a[i + 2] = a[2 * (i + 2)] + a[2 * (i + 2) + 1];
            a[i + 3] = a[2 * (i + 3)] + a[2 * (i + 3) + 1];
            a[i + 4] = a[2 * (i + 4)] + a[2 * (i + 4) + 1];
            a[i + 5] = a[2 * (i + 5)] + a[2 * (i + 5) + 1];
            a[i + 6] = a[2 * (i + 6)] + a[2 * (i + 6) + 1];
            a[i + 7] = a[2 * (i + 7)] + a[2 * (i + 7) + 1];
        }
    sum = a[0];
}



int main() {
    cout << "问题规模： " << N << endl;
    do {
        random_init();
        gettimeofday(&head, NULL);
        chain();
        gettimeofday(&tail, NULL);
        counter++;
        timeval += ((tail.tv_sec * 1000000 + tail.tv_usec) - (head.tv_sec * 1000000 + head.tv_usec)) / 1000.0;
    } while (timeval < 25);
    cout << "chain(average): " << timeval / counter << "ms" << "重复次数:  " << counter << endl;
    do {
        random_init();
        gettimeofday(&head, NULL);
        chain_unroll();
        gettimeofday(&tail, NULL);
        counter++;
        timeval += ((tail.tv_sec * 1000000 + tail.tv_usec) - (head.tv_sec * 1000000 + head.tv_usec)) / 1000.0;
    } while (timeval < 25);
    cout << "chain_unroll(average): " << timeval / counter << "ms" << "重复次数:  " << counter << endl;
    sum = 0; counter = 0; timeval = 0;
    do {
        random_init();
        gettimeofday(&head, NULL);
        doublechain_unroll();
        gettimeofday(&tail, NULL);
        counter++;
        timeval += ((tail.tv_sec * 1000000 + tail.tv_usec) - (head.tv_sec * 1000000 + head.tv_usec)) / 1000.0;
    } while (timeval < 25);
    cout << "doublechain_unroll(average): " << timeval / counter << "ms" << "重复次数:  " << counter << endl;
    sum = 0; counter = 0; timeval = 0;
    do {
        random_init();
        gettimeofday(&head, NULL);
        recursion_unroll(N);
        gettimeofday(&tail, NULL);
        counter++;
        timeval += ((tail.tv_sec * 1000000 + tail.tv_usec) - (head.tv_sec * 1000000 + head.tv_usec)) / 1000.0;
    } while (timeval < 25);
    cout << "recursion_unroll(average): " << timeval / counter << "ms" << "重复次数:  " << counter << endl;
    sum = 0; counter = 0; timeval = 0; memcpy(a, b, N * sizeof(T));
    do {
        random_init();
        gettimeofday(&head, NULL);
        circulation_unroll();
        gettimeofday(&tail, NULL);
        counter++;
        timeval += ((tail.tv_sec * 1000000 + tail.tv_usec) - (head.tv_sec * 1000000 + head.tv_usec)) / 1000.0;
    } while (timeval < 25);
    cout << "circulation_unroll(average): " << timeval / counter << "ms" << "重复次数:  " << counter << endl;
    sum = 0; counter = 0; timeval = 0; memcpy(a, b, N * sizeof(T));
}
