/*OpenMP+AVX512*/
//本文件不包含计时代码
#include "SquareMatrix.h"

#include <nmmintrin.h> //SSE 1~4.2
#include <immintrin.h> //AVX、AVX2

// OpenMP+AVX512
void omp_AVX512(SquareMatrix &mat, int N)
{
    int i, j, k;
    __m512 vkk, vkj, vik, vij;
#pragma omp parallel num_threads(NUM_THREADS), private(i, j, k, vkk, vkj, vik, vij)
    for (k = 0; k < N; k++)
    {
#pragma omp single
        {
            // AVX512向量化除法操作
            vkk = _mm512_set1_ps(mat[k][k]);
            j = k + 1;
            for (; j + 16 <= N; j += 16)
            {
                vkj = _mm512_loadu_ps(&mat[k][j]);
                vkj = _mm512_div_ps(vkj, vkk);
                _mm512_storeu_ps(&mat[k][j], vkj);
            }
            for (; j < N; j++)
            {
                mat[k][j] /= mat[k][k];
            }
            mat[k][k] = 1.0;
        }
#pragma omp for
        for (i = k + 1; i < N; i++)
        {
            //每行的消去AVX512向量化
            vik = _mm512_set1_ps(mat[i][k]);
            j = k + 1;
            for (; j + 16 <= N; j += 16)
            {
                vkj = _mm512_loadu_ps(&mat[k][j]);
                vij = _mm512_loadu_ps(&mat[i][j]);
                vij = _mm512_sub_ps(vij, _mm512_mul_ps(vik, vkj));
                _mm512_storeu_ps(&mat[i][j], vij);
            }
            for (; j < N; j++)
                mat[i][j] -= mat[i][k] * mat[k][j];
            mat[i][k] = 0.0;
        }
    }
}

int main()
{
    int N = 2048;
    SquareMatrix mat(N);
    omp_AVX512(mat, N);
}