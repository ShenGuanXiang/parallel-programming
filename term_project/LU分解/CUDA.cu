/*CUDA并行*/
//本文件编译指令：nvcc -g -o CUDA CUDA.cu
#include "SquareMatrix.h"
#include <chrono>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

const int N_arr[10] = {16, 32, 64, 128, 256, 512, 1024, 2048, 3072, 4096}; //矩阵行/列数
const int total_count_arr[10] = {10000, 1000, 200, 25, 3, 1, 1, 1, 1, 1};  //测量总次数

using namespace std;

dim3 dimofgrid(4096, 1, 1);
dim3 dimofblock(1024, 1, 1);

__global__ void division_kernel(float *data, int N, int k)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = gridDim.x * blockDim.x;
    for (int i = k + 1 + idx; i < N; i += stride)
    {
        data[k * N + i] /= data[k * N + k];
    }
}

__global__ void eliminate_kernel(float *data, int N, int k)
{
    for (int i = k + 1 + blockIdx.x; i < N; i += gridDim.x)
    {
        for (int j = k + 1 + threadIdx.x; j < N; j += blockDim.x)
        {
            data[i * N + j] -= data[i * N + k] * data[k * N + j];
        }
        __syncthreads(); //块内同步
        if (threadIdx.x == 0)
        {
            data[i * N + k] = 0;
        }
    }
}

// CUDA算法
void LU_Cuda(SquareMatrix &mat, int N)
{
    float *dev_data; //严格来说，是CPU和GPU共享的数据
    cudaError_t err;
    err = cudaMallocManaged(&dev_data, N * N * sizeof(float));
    if (err != cudaSuccess)
        printf("cudaMallocManaged failed!\n");
    memcpy(dev_data, &mat[0][0], N * N * sizeof(float));
    for (int k = 0; k < N; k++)
    {
        division_kernel<<<dimofgrid, dimofblock>>>(dev_data, N, k);
        cudaDeviceSynchronize(); //各线程同步
        dev_data[k * N + k] = 1.0;
        err = cudaGetLastError();
        if (err != cudaSuccess)
        {
            printf("division_kernel failed, %s\n", cudaGetErrorString(err));
        }
        eliminate_kernel<<<dimofgrid, dimofblock>>>(dev_data, N, k);
        cudaDeviceSynchronize();
        err = cudaGetLastError();
        if (err != cudaSuccess)
        {
            printf("elimination_kernel failed, %s\n", cudaGetErrorString(err));
        }
    }
    memcpy(&mat[0][0], dev_data, N * N * sizeof(float));
    err = cudaFree(dev_data);
    if (err != cudaSuccess)
        printf("cudaFree failed!\n");
}

int main(int argc, char *argv[])
{
    //测试CUDA算法
    for (int i = 4; i < 8; i++)
    {
        int N = N_arr[i];
        double total_time = 0, counter = 0;
        do
        {
            SquareMatrix mat(N);

            auto start = chrono::high_resolution_clock::now();
            // cudaEvent_t start, end; //计时器
            // float elapsedTime = 0.0;
            // cudaEventCreate(&start);
            // cudaEventCreate(&end);
            // cudaEventRecord(start, 0); //开始计时

            LU_Cuda(mat, N);

            // cudaEventRecord(end, 0);
            // cudaEventSynchronize(end); //停止计时
            // cudaEventElapsedTime(&elapsedTime, start, end);
            // cudaEventDestroy(start), cudaEventDestroy(end); //销毁计时器
            auto end = chrono::high_resolution_clock::now();
            total_time += chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
        } while (counter < total_count_arr[i]);
        printf("CUDA: N = %d, Average_time = %.9lf s\n", N_arr[i], total_time / counter);
    }

    return 0;
}