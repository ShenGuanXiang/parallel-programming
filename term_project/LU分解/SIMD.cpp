#include "SquareMatrix.h"
#include <chrono>

#include <nmmintrin.h> //SSE 1~4.2
#include <immintrin.h> //AVX、AVX2

const int N_arr[9] = {16, 32, 64, 128, 256, 512, 1024, 2048, 3072};   //矩阵行/列数
const int total_count_arr[9] = {10000, 1000, 200, 25, 3, 1, 1, 1, 1}; //测量总次数

void LU_SSE_unaligned(SquareMatrix &mat, int N)
{
    for (int k = 0; k < N; k++)
    {
        __m128 vkk = _mm_set1_ps(mat[k][k]);
        int j = k + 1;
        for (; j + 4 <= N; j += 4)
        {
            __m128 vkj = _mm_loadu_ps(&mat[k][j]);
            vkj = _mm_div_ps(vkj, vkk);
            _mm_storeu_ps(&mat[k][j], vkj);
        }
        for (; j < N; j++)
            mat[k][j] /= mat[k][k];
        mat[k][k] = 1.0;
        for (int i = k + 1; i < N; i++)
        {
            __m128 vik = _mm_set1_ps(mat[i][k]);
            int j = k + 1;
            for (; j + 4 <= N; j += 4)
            {
                __m128 vkj = _mm_loadu_ps(&mat[k][j]);
                __m128 vij = _mm_loadu_ps(&mat[i][j]);
                vij = _mm_sub_ps(vij, _mm_mul_ps(vik, vkj));
                _mm_storeu_ps(&mat[i][j], vij);
            }
            for (; j < N; j++)
                mat[i][j] -= mat[i][k] * mat[k][j];
            mat[i][k] = 0.0;
        }
    }
}

void LU_AVX_unaligned(SquareMatrix &mat, int N)
{
    for (int k = 0; k < N; k++)
    {
        __m256 vkk = _mm256_set1_ps(mat[k][k]);
        int j = k + 1;
        for (; j + 8 <= N; j += 8)
        {
            __m256 vkj = _mm256_loadu_ps(&mat[k][j]);
            vkj = _mm256_div_ps(vkj, vkk);
            _mm256_storeu_ps(&mat[k][j], vkj);
        }
        for (; j < N; j++)
            mat[k][j] /= mat[k][k];
        mat[k][k] = 1.0;
        for (int i = k + 1; i < N; i++)
        {
            __m256 vik = _mm256_set1_ps(mat[i][k]);
            int j = k + 1;
            for (; j + 8 <= N; j += 8)
            {
                __m256 vkj = _mm256_loadu_ps(&mat[k][j]);
                __m256 vij = _mm256_loadu_ps(&mat[i][j]);
                vij = _mm256_sub_ps(vij, _mm256_mul_ps(vik, vkj));
                _mm256_storeu_ps(&mat[i][j], vij);
            }
            for (; j < N; j++)
                mat[i][j] -= mat[i][k] * mat[k][j];
            mat[i][k] = 0.0;
        }
    }
}

void LU_AVX512_unaligned(SquareMatrix &mat, int N)
{
    for (int k = 0; k < N; k++)
    {
        __m512 vkk = _mm512_set1_ps(mat[k][k]);
        int j = k + 1;
        for (; j + 16 <= N; j += 16)
        {
            __m512 vkj = _mm512_loadu_ps(&mat[k][j]);
            vkj = _mm512_div_ps(vkj, vkk);
            _mm512_storeu_ps(&mat[k][j], vkj);
        }
        for (; j < N; j++)
            mat[k][j] /= mat[k][k];
        mat[k][k] = 1.0;
        for (int i = k + 1; i < N; i++)
        {
            __m512 vik = _mm512_set1_ps(mat[i][k]);
            int j = k + 1;
            for (; j + 16 <= N; j += 16)
            {
                __m512 vkj = _mm512_loadu_ps(&mat[k][j]);
                __m512 vij = _mm512_loadu_ps(&mat[i][j]);
                vij = _mm512_sub_ps(vij, _mm512_mul_ps(vik, vkj));
                _mm512_storeu_ps(&mat[i][j], vij);
            }
            for (; j < N; j++)
                mat[i][j] -= mat[i][k] * mat[k][j];
            mat[i][k] = 0.0;
        }
    }
}

void LU_SSE_aligned(SquareMatrix &mat, int N)
{
    for (int k = 0; k < N; k++)
    {
        __m128 vkk = _mm_set1_ps(mat[k][k]);
        int j = k + 1;
        for (; ((unsigned long long)&mat[k][j]) % 16; j++)
            mat[k][j] /= mat[k][k];
        for (; j + 4 <= N; j += 4)
        {
            __m128 vkj = _mm_load_ps(&mat[k][j]);
            vkj = _mm_div_ps(vkj, vkk);
            _mm_store_ps(&mat[k][j], vkj);
        }
        for (; j < N; j++)
            mat[k][j] /= mat[k][k];
        mat[k][k] = 1.0;
        for (int i = k + 1; i < N; i++)
        {
            __m128 vik = _mm_set1_ps(mat[i][k]);
            int j = k + 1;
            for (; ((unsigned long long)&mat[i][j]) % 16; j++)
                mat[i][j] -= mat[i][k] * mat[k][j];
            if (((i - k) * N) % 4 != 0) //这种情况无法保证k号行和i号行同时对齐，这里优先对齐i号行
                for (; j + 4 <= N; j += 4)
                {
                    __m128 vkj = _mm_loadu_ps(&mat[k][j]);
                    __m128 vij = _mm_load_ps(&mat[i][j]);
                    vij = _mm_sub_ps(vij, _mm_mul_ps(vik, vkj));
                    _mm_store_ps(&mat[i][j], vij);
                }
            else
                for (; j + 4 <= N; j += 4)
                {
                    __m128 vkj = _mm_load_ps(&mat[k][j]);
                    __m128 vij = _mm_load_ps(&mat[i][j]);
                    vij = _mm_sub_ps(vij, _mm_mul_ps(vik, vkj));
                    _mm_store_ps(&mat[i][j], vij);
                }
            for (; j < N; j++)
                mat[i][j] -= mat[i][k] * mat[k][j];
            mat[i][k] = 0.0;
        }
    }
}

void LU_AVX_aligned(SquareMatrix &mat, int N)
{
    for (int k = 0; k < N; k++)
    {
        __m256 vkk = _mm256_set1_ps(mat[k][k]);
        int j = k + 1;
        for (; ((unsigned long long)&mat[k][j]) % 32; j++)
            mat[k][j] /= mat[k][k];
        for (; j + 8 <= N; j += 8)
        {
            __m256 vkj = _mm256_load_ps(&mat[k][j]);
            vkj = _mm256_div_ps(vkj, vkk);
            _mm256_store_ps(&mat[k][j], vkj);
        }
        for (; j < N; j++)
            mat[k][j] /= mat[k][k];
        mat[k][k] = 1.0;
        for (int i = k + 1; i < N; i++)
        {
            __m256 vik = _mm256_set1_ps(mat[i][k]);
            int j = k + 1;
            for (; ((unsigned long long)&mat[i][j]) % 32; j++)
                mat[i][j] -= mat[i][k] * mat[k][j];
            if (((i - k) * N) % 8 != 0) //这种情况无法保证k号行和i号行同时对齐，这里优先对齐i号行
                for (; j + 8 <= N; j += 8)
                {
                    __m256 vkj = _mm256_loadu_ps(&mat[k][j]);
                    __m256 vij = _mm256_load_ps(&mat[i][j]);
                    vij = _mm256_sub_ps(vij, _mm256_mul_ps(vik, vkj));
                    _mm256_store_ps(&mat[i][j], vij);
                }
            else
                for (; j + 8 <= N; j += 8)
                {
                    __m256 vkj = _mm256_load_ps(&mat[k][j]);
                    __m256 vij = _mm256_load_ps(&mat[i][j]);
                    vij = _mm256_sub_ps(vij, _mm256_mul_ps(vik, vkj));
                    _mm256_store_ps(&mat[i][j], vij);
                }
            for (; j < N; j++)
                mat[i][j] -= mat[i][k] * mat[k][j];
            mat[i][k] = 0.0;
        }
    }
}

void LU_AVX512_aligned(SquareMatrix &mat, int N)
{
    for (int k = 0; k < N; k++)
    {
        __m512 vkk = _mm512_set1_ps(mat[k][k]);
        int j = k + 1;
        for (; ((unsigned long long)&mat[k][j]) % 64; j++) //串行处理到对齐边界
            mat[k][j] /= mat[k][k];
        for (; j + 16 <= N; j += 16)
        {
            __m512 vkj = _mm512_load_ps(&mat[k][j]);
            vkj = _mm512_div_ps(vkj, vkk);
            _mm512_store_ps(&mat[k][j], vkj);
        }
        for (; j < N; j++) //串行处理剩余部分的除法
            mat[k][j] /= mat[k][k];
        mat[k][k] = 1.0;
        for (int i = k + 1; i < N; i++)
        {
            __m512 vik = _mm512_set1_ps(mat[i][k]);
            int j = k + 1;
            for (; ((unsigned long long)&mat[i][j]) % 64; j++) //串行处理到对齐边界
                mat[i][j] -= mat[i][k] * mat[k][j];
            if (((i - k) * N) % 16 != 0) //这种情况无法保证k号行和i号行同时对齐，k号行沿用不对齐的存取指令
                for (; j + 16 <= N; j += 16)
                {
                    __m512 vkj = _mm512_loadu_ps(&mat[k][j]);
                    __m512 vij = _mm512_load_ps(&mat[i][j]);
                    vij = _mm512_sub_ps(vij, _mm512_mul_ps(vik, vkj));
                    _mm512_store_ps(&mat[i][j], vij);
                }
            else
                for (; j + 16 <= N; j += 16)
                {
                    __m512 vkj = _mm512_load_ps(&mat[k][j]);
                    __m512 vij = _mm512_load_ps(&mat[i][j]);
                    vij = _mm512_sub_ps(vij, _mm512_mul_ps(vik, vkj));
                    _mm512_store_ps(&mat[i][j], vij);
                }
            for (; j < N; j++) //串行处理剩余部分的消去
                mat[i][j] -= mat[i][k] * mat[k][j];
            mat[i][k] = 0.0;
        }
    }
}

int main(int argc, char *argv[])
{
    //测试SSE
    for (int i = 0; i < sizeof(N_arr) / sizeof(int); i++)
    {
        int N = N_arr[i];
        double total_time = 0, counter = 0;
        do
        {
            SquareMatrix mat(N);
            auto start = std::chrono::high_resolution_clock::now();

            LU_SSE_unaligned(mat, N);

            auto end = std::chrono::high_resolution_clock::now();
            total_time += std::chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
        } while (counter < total_count_arr[i]);
        printf("SSE_unaligned: N = %d, Average_time = %.9lf s\n", N_arr[i], total_time / counter);
    }
    for (int i = 0; i < sizeof(N_arr) / sizeof(int); i++)
    {
        int N = N_arr[i];
        double total_time = 0, counter = 0;
        do
        {
            SquareMatrix mat(N);
            auto start = std::chrono::high_resolution_clock::now();

            LU_SSE_aligned(mat, N);

            auto end = std::chrono::high_resolution_clock::now();
            total_time += std::chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
        } while (counter < total_count_arr[i]);
        printf("SSE_aligned: N = %d, Average_time = %.9lf s\n", N_arr[i], total_time / counter);
    }
    //测试AVX
    for (int i = 0; i < sizeof(N_arr) / sizeof(int); i++)
    {
        int N = N_arr[i];
        double total_time = 0, counter = 0;
        do
        {
            SquareMatrix mat(N);
            auto start = std::chrono::high_resolution_clock::now();

            LU_AVX_unaligned(mat, N);

            auto end = std::chrono::high_resolution_clock::now();
            total_time += std::chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
        } while (counter < total_count_arr[i]);
        printf("AVX_unaligned: N = %d, Average_time = %.9lf s\n", N_arr[i], total_time / counter);
    }
    for (int i = 0; i < sizeof(N_arr) / sizeof(int); i++)
    {
        int N = N_arr[i];
        double total_time = 0, counter = 0;
        do
        {
            SquareMatrix mat(N);
            auto start = std::chrono::high_resolution_clock::now();

            LU_AVX_aligned(mat, N);

            auto end = std::chrono::high_resolution_clock::now();
            total_time += std::chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
        } while (counter < total_count_arr[i]);
        printf("AVX_aligned: N = %d, Average_time = %.9lf s\n", N_arr[i], total_time / counter);
    }
    //测试AVX512
    for (int i = 0; i < sizeof(N_arr) / sizeof(int); i++)
    {
        int N = N_arr[i];
        double total_time = 0, counter = 0;
        do
        {
            SquareMatrix mat(N);
            auto start = std::chrono::high_resolution_clock::now();

            LU_AVX512_unaligned(mat, N);

            auto end = std::chrono::high_resolution_clock::now();
            total_time += std::chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
        } while (counter < total_count_arr[i]);
        printf("AVX512_unaligned: N = %d, Average_time = %.9lf s\n", N_arr[i], total_time / counter);
    }
    for (int i = 0; i < sizeof(N_arr) / sizeof(int); i++)
    {
        int N = N_arr[i];
        double total_time = 0, counter = 0;
        do
        {
            SquareMatrix mat(N);
            auto start = std::chrono::high_resolution_clock::now();

            LU_AVX512_aligned(mat, N);

            auto end = std::chrono::high_resolution_clock::now();
            total_time += std::chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
        } while (counter < total_count_arr[i]);
        printf("AVX512_aligned: N = %d, Average_time = %.9lf s\n", N_arr[i], total_time / counter);
    }

    return 0;
}
