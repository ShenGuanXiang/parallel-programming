/*串行*/
#include "SquareMatrix.h"
#include <chrono>

const int N_arr[10] = {16, 32, 64, 128, 256, 512, 1024, 2048, 3072, 4096}; //矩阵行/列数
const int total_count_arr[10] = {10000, 1000, 200, 25, 3, 1, 1, 1, 1, 1};  //测量总次数

//串行算法
void LU_Serial(SquareMatrix &mat, int N)
{
    for (int k = 0; k < N; k++)
    {
        for (int j = k + 1; j < N; j++)
            mat[k][j] /= mat[k][k];
        mat[k][k] = 1.0;
        for (int i = k + 1; i < N; i++)
        {
            for (int j = k + 1; j < N; j++)
                mat[i][j] -= mat[i][k] * mat[k][j];
            mat[i][k] = 0.0;
        }
    }
}

int main(int argc, char *argv[])
{
    //测试串行算法
    for (int i = 0; i < sizeof(N_arr) / sizeof(int); i++)
    {
        int N = N_arr[i];
        double total_time = 0, counter = 0;
        do
        {
            SquareMatrix mat(N, true);

            auto start = std::chrono::high_resolution_clock::now();

            LU_Serial(mat, N);

            auto end = std::chrono::high_resolution_clock::now();
            total_time += std::chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
        } while (counter < total_count_arr[i]);
        printf("Serial: N = %d, Average_time = %.9lf s\n", N_arr[i], total_time / counter);
    }
    return 0;
}