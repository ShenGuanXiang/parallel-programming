﻿/*基类：方阵类，包含分配回收空间、初始化等基本功能*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

//浮点型二维方阵类
class SquareMatrix
{

private:
    float *data;
    int N;

public:
    //构造矩阵
    void init()
    {
        //构造随机单位上三角矩阵
        const int MIN = -2, MAX = 2; //随机数生成范围，设的比较小，防止溢出
        srand((unsigned)time(NULL));
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < i; j++)
                data[i * N + j] = 0.0;
            data[i * N + i] = 1.0;
            for (int j = i + 1; j < N; j++)
            {
                data[i * N + j] = MIN + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (MAX - MIN)));
                while (-0.1 < data[i * N + j] && data[i * N + j] < 0.1)
                    data[i * N + j] = MIN + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (MAX - MIN)));
            }
        }
        //进行初等行变换
        for (int k = 0; k < N; k++)
        {
            for (int i = k + 1; i < N; i++)
            {
                bool overflow = false;
                //先检验两行相加后是否有绝对值较大或较小的，再相加
                for (int j = 0; j < N; j++)
                    if (data[i * N + j] + data[k * N + j] < -10 || data[i * N + j] + data[k * N + j] > 10 || (-0.05 < data[i * N + j] + data[k * N + j] < 0.05))
                    {
                        overflow = true;
                        break;
                    }
                if (!overflow)
                {
                    for (int j = 0; j < N; j++)
                        data[i * N + j] += data[k * N + j];
                }
            }
        }
    }

    SquareMatrix()
        : N(0), data(NULL)
    {
    }

    SquareMatrix(int N, bool init_data = true)
        : N(N), data(NULL)
    {
        data = new float[N * N];
        if (init_data)
            init();
    }

    SquareMatrix(const SquareMatrix &another)
        : N(another.N), data(NULL)
    {
        data = new float[N * N];
        memcpy(data, another.data, N * N * sizeof(float));
    }

    ~SquareMatrix()
    {
        if (data != NULL)
        {
            delete[] data;
            data = NULL;
        }
    }

    //返回二维数组的第 i 行地址
    float *const operator[](int i)
    {
        return &data[i * N];
    }

    void print()
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                printf("%.5lf ", data[i * N + j]);
            }
            printf("\n");
        }
        printf("\n");
    }
};