/*MPI块循环划分+流水线*/
//本文件在配好MPI环境的VS2019中编译运行
#include "SquareMatrix.h"
#include <chrono>

#include <mpi.h>

const int N_arr[10] = {16, 32, 64, 128, 256, 512, 1024, 2048, 3072, 4096}; //矩阵行/列数
const int total_count_arr[10] = {10000, 1000, 200, 25, 3, 1, 1, 1, 1, 1};  //测量总次数

using namespace std;

int main(int argc, char *argv[])
{
    int rank, NUM_PROCS;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &NUM_PROCS);
    //测试不同矩阵规模下MPI块划分算法的平均执行时间
    for (int p = 0; p < 10; p++)
    {
        int N = N_arr[p];
        double total_time = 0, counter = 0;
        SquareMatrix mat(N, false);
        do
        {
            MPI_Barrier(MPI_COMM_WORLD);
            if (rank == 0)
                mat.init();
            MPI_Bcast(&mat[0][0], N * N, MPI_FLOAT, 0, MPI_COMM_WORLD);

            MPI_Barrier(MPI_COMM_WORLD);
            auto start = std::chrono::high_resolution_clock::now();

            int d = N / NUM_PROCS;                                                           //块厚度
            int r1 = rank * d, r2 = r1 + d - 1, r3 = rank < (N % NUM_PROCS) ? N - rank : -1; //本进程负责的行:[r1,r2] and r3
            for (int k = 0; k < N; k++)
            {
                int rank1 = k / d < NUM_PROCS ? k / d : N - 1 - k; //负责k号行的进程
                if (rank == rank1)
                {
                    for (int j = k + 1; j < N; j++)
                        mat[k][j] /= mat[k][k];
                    mat[k][k] = 1.0;
                }
                MPI_Bcast(&mat[k][k], N - k, MPI_FLOAT, rank1, MPI_COMM_WORLD); //如果像这样不传一整行，消元后的矩阵的下三角部分会出现非零值，忽略即可
                for (int i = max(r1, k + 1); i < r2; i++)
                {
                    for (int j = k + 1; j < N; j++)
                        mat[i][j] -= mat[i][k] * mat[k][j];
                    mat[i][k] = 0.0;
                }
                if (r3 > k)
                {
                    for (int j = k + 1; j < N; j++)
                        mat[r3][j] -= mat[r3][k] * mat[r3][j];
                    mat[r3][k] = 0.0;
                }
            }

            MPI_Barrier(MPI_COMM_WORLD);
            auto end = std::chrono::high_resolution_clock::now();
            total_time += std::chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
        } while (counter < total_count_arr[p]);

        if (rank == 0)
        {
            printf("MPI: N = %d, NUM_PROCS = %d, Average_Time = %.9lf s\n", N, NUM_PROCS, total_time / counter);
        }
    }
    //测试不同矩阵规模下MPI块划分+流水线算法的平均执行时间
    for (int p = 0; p < 10; p++)
    {
        int N = N_arr[p];
        double total_time = 0, counter = 0;
        SquareMatrix mat(N, false);
        do
        {
            MPI_Barrier(MPI_COMM_WORLD);
            if (rank == 0)
                mat.init();
            MPI_Bcast(&mat[0][0], N * N, MPI_FLOAT, 0, MPI_COMM_WORLD);

            MPI_Barrier(MPI_COMM_WORLD);
            auto start = std::chrono::high_resolution_clock::now();

            int d = N / NUM_PROCS;                                                           //块厚度
            int r1 = rank * d, r2 = r1 + d - 1, r3 = rank < (N % NUM_PROCS) ? N - rank : -1; //本进程负责的行:[r1,r2] and r3
            for (int k = 0; k < N; k++)
            {
                int rank1 = k / d < NUM_PROCS ? k / d : N - 1 - k; //负责k号行的进程
                if (rank != rank1)
                    MPI_Recv(&mat[k][k], N - k, MPI_FLOAT, rank ? rank - 1 : NUM_PROCS - 1, k, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                if (rank == rank1)
                {
                    for (int j = k + 1; j < N; j++)
                        mat[k][j] /= mat[k][k];
                    mat[k][k] = 1.0;
                }
                if (rank != (k % NUM_PROCS ? k % NUM_PROCS - 1 : NUM_PROCS - 1))
                    MPI_Send(&mat[k][k], N - k, MPI_FLOAT, (rank + 1) % NUM_PROCS, k, MPI_COMM_WORLD);
                for (int i = max(r1, k + 1); i < r2; i++)
                {
                    for (int j = k + 1; j < N; j++)
                        mat[i][j] -= mat[i][k] * mat[k][j];
                    mat[i][k] = 0.0;
                }
                if (r3 > k)
                {
                    for (int j = k + 1; j < N; j++)
                        mat[r3][j] -= mat[r3][k] * mat[r3][j];
                    mat[r3][k] = 0.0;
                }
            }

            MPI_Barrier(MPI_COMM_WORLD);
            auto end = std::chrono::high_resolution_clock::now();
            total_time += std::chrono::duration<double, std::ratio<1, 1>>(end - start).count(), counter++;
        } while (counter < total_count_arr[p]);

        if (rank == 0)
        {
            printf("MPI_pipeline: N = %d, NUM_PROCS = %d, Average_Time = %.9lf s\n", N, NUM_PROCS, total_time / counter);
        }
    }

    MPI_Finalize();

    return 0;
}