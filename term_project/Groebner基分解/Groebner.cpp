﻿/*Groebner基分解中的高斯消去*/
//本文件编译指令：g++ -g -o Groebner Groebner.cpp -march=native -lpthread
#include <stdio.h>
#include <cstring>
#include <fstream>
#include <sstream>
#include <chrono>
#include <unordered_map>

#include <omp.h>
#include <nmmintrin.h>
#include <immintrin.h>
#include <pthread.h>
#include <thread>

using namespace std;

const int N_arr[11] = {130, 254, 562, 1011, 2362, 3799, 8399, 23075, 37960, 43577, 85401};																																																																	 //矩阵列数
const int m_arr[11] = {8, 53, 53, 263, 453, 1953, 4535, 14325, 14921, 54274, 756};																																																																			 //被消元行总行数
const int total_count_arr[11] = {3000, 40, 25, 25, 3, 3, 3, 1, 1, 1, 1};																																																																					 //测量总次数
string eliminators_txt[11] = {"1_130_22_8/1.txt", "2_254_106_53/1.txt", "3_562_170_53/1.txt", "4_1011_539_263/1.txt", "5_2362_1226_453/1.txt", "6_3799_2759_1953/1.txt", "7_8399_6375_4535/1.txt", "8_23075_18748_14325/tmp1.txt", "9_37960_29304_14921/tmp1.txt", "10_43577_39477_54274/tmp1.txt", "11_85401_5724_756/tmp1.txt"};							 //消元子文件路径
string eliminated_rows_txt[11] = {"1_130_22_8/2.txt", "2_254_106_53/2.txt", "3_562_170_53/2.txt", "4_1011_539_263/2.txt", "5_2362_1226_453/2.txt", "6_3799_2759_1953/2.txt", "7_8399_6375_4535/2.txt", "8_23075_18748_14325/2.txt", "9_37960_29304_14921/2.txt", "10_43577_39477_54274/2.txt", "11_85401_5724_756/2.txt"};									 //被消元行文件路径
string real_res_txt[11] = {"1_130_22_8/3.txt", "2_254_106_53/3.txt", "3_562_170_53/3.txt", "4_1011_539_263/3.txt", "5_2362_1226_453/3.txt", "6_3799_2759_1953/3.txt", "7_8399_6375_4535/3.txt", "8_23075_18748_14325/3.txt", "9_37960_29304_14921/3.txt", "10_43577_39477_54274/3.txt", "11_85401_5724_756/3.txt"};											 //正确的消元结果文件路径
string myres_txt[11] = {"1_130_22_8/myres.txt", "2_254_106_53/myres.txt", "3_562_170_53/myres.txt", "4_1011_539_263/myres.txt", "5_2362_1226_453/myres.txt", "6_3799_2759_1953/myres.txt", "7_8399_6375_4535/myres.txt", "8_23075_18748_14325/myres.txt", "9_37960_29304_14921/myres.txt", "10_43577_39477_54274/myres.txt", "11_85401_5724_756/myres.txt"}; //计算的消元结果文件路径
const int len_arr[11] = {5, 8, 18, 32, 74, 119, 263, 722, 1187, 1362, 2669};																																																																				 // Mybitset中bits数组元素个数(N/32向上取整)

//一个类对象代表一行消元子/被消元行，bit位从右（高地址）向左（低地址）开始从0递增
class Mybitset
{
public:
	int len;	   //整型元素个数
	int lp;		   //首项
	bool upgraded; //指示被消元行是否升格
	int *bits;	   //位向量
	// Mybitset()
	// 	: len(0), lp(-1), upgraded(false), bits(NULL)
	// {
	// }
	Mybitset(int len)
		: len(len), lp(-1), upgraded(false), bits(NULL)
	{
		this->bits = new int[len];
	}
	// Mybitset(const Mybitset* another) {
	// 	this->len = another->len;
	// 	this->lp = another->lp;
	// 	this->bits = new int[len];
	// 	this->upgraded = another->upgraded;
	// 	memcpy(this->bits, another->bits, len * sizeof(len));
	// }
	~Mybitset()
	{
		if (bits != NULL)
		{
			delete[] bits;
			bits = NULL;
		}
	}
	//获取一位在bits数组中的索引
	int index(int pos)
	{
		return len - 1 - pos / 32;
	}
	//置位
	void set(int pos, bool value)
	{
		if (value == 1)
			bits[index(pos)] |= value << (pos % 32);
		else
			bits[index(pos)] &= (~(value << (pos % 32)));
	}
	//获取一位的值
	bool get(int pos)
	{
		return (bits[index(pos)]) & (1 << (pos % 32));
	}
	//全部置0
	void set_all_zero()
	{
		memset(bits, 0, len * sizeof(int));
	}
	// //判断是否相等
	// bool equal(Mybitset *another)
	// {
	// 	for (int i = 0; i < len; i++)
	// 		if (bits[i] != another->bits[i])
	// 			return false;
	// 	return true;
	// }
	//打印
	void print1()
	{
		for (int i = 0; i < len; i++)
			for (int j = 0; j < 32; j++)
				printf("%d", ((bits[i] >> (31 - j)) & 1) != 0);
		printf("\n");
	}
	void print2()
	{
		printf("lp=%d\n", lp);
		for (int j = len * 32 - 1; j >= 0; j--)
		{
			if (get(j))
				printf("%d ", j);
		}
		printf("\n");
	}
};

Mybitset **eliminators = NULL;					 // eliminator:所有消元子构成的集合，eliminator[i]:首项为i的消元子, NULL代表没有对应消元子;
Mybitset **eliminated_rows = NULL;				 // eliminated_row:所有被消元行构成的数组
unordered_map<int, fstream::pos_type> lp2offset; //消元子首项->该消元子在文件中的首地址偏移(批量处理时用)
unordered_map<int, fstream::pos_type> id2offset; //被消元行行号->该被消元行在文件中的首地址偏移(批量处理时用)

//为消元子和被消元行分配空间，每个消元子、被消元行均初始化为空指针
void malloc_(int SampleId)
{
	int m = m_arr[SampleId], N = N_arr[SampleId];
	//需满足eliminated_rows == NULL
	eliminated_rows = new Mybitset *[m];
	for (int i = 0; i < m; i++)
		eliminated_rows[i] = NULL;
	//需满足eliminators == NULL
	eliminators = new Mybitset *[N];
	for (int i = 0; i < N; i++)
		eliminators[i] = NULL;
}

//释放所有消元子、被消元行
void free_(int SampleId)
{
	int N = N_arr[SampleId], m = m_arr[SampleId];
	//释放消元子
	if (eliminators != NULL)
	{
		for (int i = 0; i < N; i++)
		{
			//升格得到的新的消元子和被消元行一起释放，而不在这里释放，防止二次释放
			if (eliminators[i] != NULL && !eliminators[i]->upgraded)
			{
				delete eliminators[i];
				eliminators[i] = NULL;
			}
		}
		delete[] eliminators;
		eliminators = NULL;
	}
	//释放被消元行
	if (eliminated_rows != NULL)
	{
		for (int i = 0; i < m; i++)
		{
			if (eliminated_rows[i] != NULL)
			{
				delete eliminated_rows[i];
				eliminated_rows[i] = NULL;
			}
		}
		delete[] eliminated_rows;
		eliminated_rows = NULL;
	}
}

//对比自己的算法的消元结果和正确的结果，验证算法正确性
void verify(int SampleId)
{
	ifstream real_res_file(real_res_txt[SampleId]), myres_file(myres_txt[SampleId]);
	string real_line, my_line;
	while (getline(real_res_file, real_line))
	{
		getline(myres_file, my_line);
		if (real_line != my_line)
		{
			printf("real_line: %s\n", real_line.c_str());
			printf("my_line: %s\n", my_line.c_str());
			printf("error!\n");
			exit(2);
		}
	}
	real_res_file.close(), myres_file.close();
}

/************************************************************************************************************************************************************************************
 * 下面的部分是批量处理的外存算法中要用到的一些函数
 ************************************************************************************************************************************************************************************/

//预处理：读一遍消元子文件，初始化lp2offset, 值为-1代表最初无相应消元子
void get_lp2offset(int SampleId, double &time)
{
	auto start = chrono::high_resolution_clock::now();

	int N = N_arr[SampleId];
	for (int i = 0; i < N; i++)
		lp2offset[i] = -1;
	ifstream eliminators_file(eliminators_txt[SampleId], ios::binary);
	string line;
	fstream::pos_type offset = eliminators_file.tellg();
	while (getline(eliminators_file, line))
	{
		istringstream ss(line);
		int lp;
		ss >> lp;
		lp2offset[lp] = offset;
		offset = eliminators_file.tellg();
	}
	eliminators_file.close(), eliminators_file.clear();

	auto end = chrono::high_resolution_clock::now();
	time += chrono::duration<double, std::ratio<1, 1>>(end - start).count();
}

//预处理：读一遍被消元行文件，初始化id2offset
void get_id2offset(int SampleId, double &time)
{
	auto start = chrono::high_resolution_clock::now();

	int m = m_arr[SampleId];
	ifstream eliminated_rows_file(eliminated_rows_txt[SampleId], ios::binary);
	string line;
	fstream::pos_type offset = eliminated_rows_file.tellg();
	for (int i = 0; i < m; i++)
	{
		getline(eliminated_rows_file, line);
		id2offset[i] = offset;
		offset = eliminated_rows_file.tellg();
	}
	eliminated_rows_file.close(), eliminated_rows_file.clear();

	auto end = chrono::high_resolution_clock::now();
	time += chrono::duration<double, std::ratio<1, 1>>(end - start).count();
}

//批量读消元子, 这些消元子首项位于[min_lp, max_lp]
void batch_read_eliminators(int max_lp, int min_lp, int SampleId, double &time)
{
	auto start = chrono::high_resolution_clock::now();

	int len = len_arr[SampleId], N = N_arr[SampleId];
	//需满足eliminators != NULL && eliminators[min_lp <= i <= max_lp] == NULL
	ifstream eliminators_file(eliminators_txt[SampleId], ios::binary);
	for (int i = max_lp; i >= min_lp; i--)
	{
		fstream::pos_type offset = lp2offset[i];
		if (offset == -1)
			continue;
		eliminators_file.clear(); //清除eof标记
		eliminators_file.seekg(offset, ios::beg);
		string line;
		getline(eliminators_file, line);
		istringstream ss(line);
		int lp;
		ss >> lp; // i = lp
		eliminators[lp] = new Mybitset(len);
		eliminators[lp]->set_all_zero();
		eliminators[lp]->set(lp, 1);
		eliminators[lp]->lp = lp;
		int pos;
		while (ss >> pos)
			eliminators[lp]->set(pos, 1);

		// eliminators[lp]->print2();
		// system("pause");
	}
	eliminators_file.close(), eliminators_file.clear();

	auto end = chrono::high_resolution_clock::now();
	time += chrono::duration<double, std::ratio<1, 1>>(end - start).count();
}

//批量读被消元行, 这些被消元行行号处于[min_id, max_id]
void batch_read_eliminated_rows(int min_id, int max_id, int SampleId, double &time)
{
	auto start = chrono::high_resolution_clock::now();

	int len = len_arr[SampleId];
	//需满足eliminators != NULL && eliminators[min_lp <= i <= max_lp] == NULL
	ifstream eliminated_rows_file(eliminated_rows_txt[SampleId], ios::binary);
	eliminated_rows_file.seekg(id2offset[min_id], ios::beg);
	for (int i = min_id; i <= max_id; i++)
	{
		string line;
		getline(eliminated_rows_file, line);
		istringstream ss(line);
		int lp;
		ss >> lp;
		eliminated_rows[i] = new Mybitset(len);
		eliminated_rows[i]->set_all_zero();
		eliminated_rows[i]->set(lp, 1);
		eliminated_rows[i]->lp = lp;
		int pos;
		while (ss >> pos)
			eliminated_rows[i]->set(pos, 1);

		// eliminated_rows[i]->print2();
		// system("pause");
	}
	eliminated_rows_file.close(), eliminated_rows_file.clear();

	auto end = chrono::high_resolution_clock::now();
	time += chrono::duration<double, std::ratio<1, 1>>(end - start).count();
}

//将新升格的消元子追加到文件末尾
void write_new_eliminator(Mybitset *new_eliminator, int SampleId, double &time)
{
	auto start = chrono::high_resolution_clock::now();

	ofstream eliminators_file(eliminators_txt[SampleId], ios::app | ios::binary);
	//保存原先文件末尾偏移
	eliminators_file.seekp(0, ios::end);
	fstream::pos_type offset = eliminators_file.tellp();
	//向文件末尾添加新的消元子
	for (int j = N_arr[SampleId] - 1; j > -1; j--)
	{
		if (new_eliminator->get(j))
			eliminators_file << j << " ";
	}
	eliminators_file << "\n";
	eliminators_file.close();
	//添加到哈希表中
	lp2offset[new_eliminator->lp] = offset;

	auto end = chrono::high_resolution_clock::now();
	time += chrono::duration<double, std::ratio<1, 1>>(end - start).count();
}

//将一批被消元行结果追加到文件末尾, 这些被消元行行号位于[min_id, max_id]
void batch_write_res(int min_id, int max_id, int SampleId, double &time)
{
	auto start = chrono::high_resolution_clock::now();

	int N = N_arr[SampleId];
	ofstream myres_file(myres_txt[SampleId], ios::app);
	for (int i = min_id; i <= max_id; i++)
	{
		for (int j = N - 1; j >= 0; j--)
			if (eliminated_rows[i]->get(j))
				myres_file << j << " ";
		myres_file << '\n';
	}
	myres_file.close(), myres_file.clear();

	auto end = chrono::high_resolution_clock::now();
	time += chrono::duration<double, std::ratio<1, 1>>(end - start).count();
}

//批量释放消元子, 这些消元子首项处于[min_lp, max_lp]
void batch_free_eliminators(int min_lp, int max_lp)
{
	//需满足eliminators != NULL
	for (int i = min_lp; i <= max_lp; i++)
		//升格得到的新的消元子和被消元行一起释放，而不在这里释放
		if (eliminators[i] != NULL && !eliminators[i]->upgraded)
		{
			delete eliminators[i];
			eliminators[i] = NULL;
		}
}

//批量释放被消元行, 这些被消元行行号处于[min_id, max_id]
void batch_free_eliminated_rows(int min_id, int max_id)
{
	//需满足eliminated_rows != NULL
	for (int i = min_id; i <= max_id; i++)
		if (eliminated_rows[i] != NULL)
		{
			delete eliminated_rows[i];
			eliminated_rows[i] = NULL;
		}
}

//串行算法1：对大规模样本, 被消元行分批次处理, 每批被消元行又由消元子分批次处理, 每批是按行序处理被消元行的。
// batch_size是每批处理的被消元行数, delta_lp是每批处理的消元子数
// time_arr[0:6]依次为计算时间, 预处理读消元子文件的时间, 预处理读被消元行文件的时间, 解析被消元行文件的时间, 解析消元子文件的时间, 新的消元子写入时间, 结果写入时间
void Serial_row_major(int batch_size, int delta_lp, int SampleId, double *time_arr)
{
	int m = m_arr[SampleId], N = N_arr[SampleId], len = len_arr[SampleId];

	get_lp2offset(SampleId, time_arr[1]), get_id2offset(SampleId, time_arr[2]);
	malloc_(SampleId);

	auto start = chrono::high_resolution_clock::now();

	for (int k = 0; k < m; k += batch_size)
	{
		int min_id = k, max_id = min(k + batch_size - 1, m - 1);
		batch_read_eliminated_rows(min_id, max_id, SampleId, time_arr[3]);
		//处理该批被消元行
		for (int max_lp = N - 1; max_lp >= 0; max_lp -= delta_lp)
		{
			int min_lp = max(max_lp - delta_lp + 1, 0);					   //保证最后一批消元子以0为下界
			batch_read_eliminators(max_lp, min_lp, SampleId, time_arr[4]); //读该批消元子
			//处理该批消元子
			for (int i = min_id; i <= max_id; i++)
			{
				int lp = eliminated_rows[i]->lp; //获取i号被消元行在本批消元前的首项
				while (lp >= min_lp && !eliminated_rows[i]->upgraded)
				{
					if (eliminators[lp] != NULL)
					{
						for (int j = eliminated_rows[i]->index(lp); j < len; j++)
							eliminated_rows[i]->bits[j] ^= eliminators[lp]->bits[j];
						//更新i号被消元行的首项
						for (int j = lp - 1; j >= -1; j--)
						{
							if (j == -1 || eliminated_rows[i]->get(j))
							{
								lp = eliminated_rows[i]->lp = j;
								break;
							}
						}
					}
					else
					{
						//升格为消元子
						eliminators[lp] = eliminated_rows[i]; //浅拷贝
						eliminated_rows[i]->upgraded = true;
						write_new_eliminator(eliminated_rows[i], SampleId, time_arr[5]); //将新的消元子写入文件
						break;
					}
				}
			}
			batch_free_eliminators(min_lp, max_lp); //释放该批消元子的空间
		}
		batch_write_res(min_id, max_id, SampleId, time_arr[6]); //将本批被消元行结果写入文件
		batch_free_eliminated_rows(min_id, max_id);				//释放该批被消元行的空间
	}

	auto end = chrono::high_resolution_clock::now();

	free_(SampleId);
	time_arr[0] += chrono::duration<double, std::ratio<1, 1>>(end - start).count() - time_arr[3] - time_arr[4] - time_arr[5] - time_arr[6];
}

/************************************************************************************************************************************************************************************
 * 下面是外存算法的并行版本
 ************************************************************************************************************************************************************************************/

//处理一批消元子和被消元行，采用AVX512优化，在此进行封装，方便直接调用
void Compute_a_batch(int min_id, int max_id, int min_lp, int max_lp, int SampleId, double &useless_time)
{
	int len = eliminated_rows[min_id]->len;
	for (int i = min_id; i <= max_id; i++)
	{
		int lp = eliminated_rows[i]->lp; //获取i号被消元行在本批消元前的首项
		while (lp >= min_lp && !eliminated_rows[i]->upgraded)
		{
			if (eliminators[lp] != NULL)
			{
				// AVX512并行异或
				int j = eliminated_rows[i]->index(lp);
				for (; j + 16 <= len; j += 16)
				{
					__m512i va = _mm512_loadu_si512(&(eliminated_rows[i]->bits[j]));
					__m512i vb = _mm512_loadu_si512(&(eliminators[lp]->bits[j]));
					va = _mm512_xor_si512(va, vb);
					_mm512_storeu_si512(&(eliminated_rows[i]->bits[j]), va);
				}
				for (; j < len; j++)
					eliminated_rows[i]->bits[j] ^= eliminators[lp]->bits[j];
				//更新i号被消元行的首项
				for (int j = lp - 1; j >= -1; j--)
				{
					if (j == -1 || eliminated_rows[i]->get(j))
					{
						lp = eliminated_rows[i]->lp = j;
						break;
					}
				}
			}
			else
			{
				//升格为消元子
				eliminators[lp] = eliminated_rows[i]; //浅拷贝
				eliminated_rows[i]->upgraded = true;
				write_new_eliminator(eliminated_rows[i], SampleId, useless_time); //将新的消元子写入文件
				break;
			}
		}
	}
}

//线程参数
struct threadParam_t
{
	int t_id; //线程id
	int batch_size, delta_lp, SampleId;
};

pthread_barrier_t barrier; //用来同步

//静态线程函数
void *threadFunc(void *param)
{
	//先获取相关参数
	threadParam_t *p = (threadParam_t *)param;
	int t_id = p->t_id, batch_size = p->batch_size, delta_lp = p->delta_lp, SampleId = p->SampleId;
	int m = m_arr[SampleId], N = N_arr[SampleId], len = len_arr[SampleId];

	double useless_time = 0; //并行算法中测试单个函数执行时间无意义，这个变量仅用作传参
	if (t_id == 0)
	{
		get_lp2offset(SampleId, useless_time), get_id2offset(SampleId, useless_time);
		malloc_(SampleId);
	}
	for (int k = 0; k < m; k += batch_size)
	{
		int min_id = k, max_id = min(k + batch_size - 1, m - 1);
		if (t_id == 0)
		{
			batch_read_eliminated_rows(min_id, max_id, SampleId, useless_time);
			batch_read_eliminators(N - 1, max(N - delta_lp, 0), SampleId, useless_time); //预取第一批消元子
		}
		//处理该批被消元行
		for (int max_lp = N - 1; max_lp >= 0; max_lp -= delta_lp)
		{
			pthread_barrier_wait(&barrier);
			int min_lp = max(max_lp - delta_lp + 1, 0); //保证最后一批消元子以0为下界
			// 0号线程负责本批次计算
			if (t_id == 0)
			{
				Compute_a_batch(min_id, max_id, min_lp, max_lp, SampleId, useless_time); //可能会写入新的消元子，但新的消元子不在下一批中
				batch_free_eliminators(min_lp, max_lp);									 //释放该批消元子的空间
			}
			// 1号线程负责下一批次的消元子读入
			else if (min_lp)
				batch_read_eliminators(min_lp - 1, max(2 * min_lp - max_lp - 1, 0), SampleId, useless_time);
		}
		if (t_id == 0)
		{
			batch_write_res(min_id, max_id, SampleId, useless_time); //将本批被消元行结果写入文件
			batch_free_eliminated_rows(min_id, max_id);				 //释放该批被消元行的空间
		}
	}
	if (t_id == 0)
		free_(SampleId);
	pthread_exit(NULL);
	return NULL;
}

//外存算法的并行版本
void Parallel_row_major(int batch_size, int delta_lp, int SampleId, double &total_time, double &useless_time)
{
	int m = m_arr[SampleId], N = N_arr[SampleId], len = len_arr[SampleId];

	auto start = chrono::high_resolution_clock::now();
	pthread_barrier_init(&barrier, NULL, 2);															 //初始化barrier
	pthread_t thread_handles[2];																		 // 创建对应的 Handle
	threadParam_t param[2] = {{0, batch_size, delta_lp, SampleId}, {1, batch_size, delta_lp, SampleId}}; // 创建对应的线程数据结构
	for (int t_id = 0; t_id < 2; t_id++)
		pthread_create(&thread_handles[t_id], NULL, threadFunc, (void *)&param[t_id]);
	for (int t_id = 0; t_id < 2; t_id++)
		pthread_join(thread_handles[t_id], NULL);
	pthread_barrier_destroy(&barrier);
	auto end = chrono::high_resolution_clock::now();
	total_time += chrono::duration<double, std::ratio<1, 1>>(end - start).count();
}

/***********************************************************************************************************************************************************************************
 * 下面的部分是一次性处理的内存算法中要用到的一些函数
 ************************************************************************************************************************************************************************************/

//读全部被消元行、消元子
void read_all(int SampleId, double &time)
{
	auto start = chrono::high_resolution_clock::now();

	int m = m_arr[SampleId], len = len_arr[SampleId];
	string line;
	//读消元子
	ifstream eliminators_file(eliminators_txt[SampleId], ios::binary);
	while (getline(eliminators_file, line))
	{
		istringstream ss(line);
		line = "";
		int lp;
		ss >> lp;
		eliminators[lp] = new Mybitset(len);
		eliminators[lp]->set_all_zero();
		eliminators[lp]->lp = lp;
		eliminators[lp]->set(lp, 1);
		if (!ss.eof())
		{
			int pos;
			while (ss >> pos)
				eliminators[lp]->set(pos, 1);
		}
	}
	eliminators_file.close(), eliminators_file.clear();
	//读被消元行
	ifstream eliminated_rows_file(eliminated_rows_txt[SampleId], ios::binary);
	for (int i = 0; i < m; i++)
	{
		getline(eliminated_rows_file, line);
		istringstream ss(line);
		line = "";
		int lp;
		ss >> lp;
		eliminated_rows[i] = new Mybitset(len);
		eliminated_rows[i]->set_all_zero();
		eliminated_rows[i]->lp = lp;
		eliminated_rows[i]->set(lp, 1);
		if (!ss.eof())
		{
			int pos;
			while (ss >> pos)
				eliminated_rows[i]->set(pos, 1);
		}
	}
	eliminated_rows_file.close(), eliminators_file.clear();

	auto end = chrono::high_resolution_clock::now();
	time += chrono::duration<double, std::ratio<1, 1>>(end - start).count();
}

//将被消元行结果全部写入文件
void write_all_res(int SampleId, double &time)
{
	auto start = chrono::high_resolution_clock::now();

	int m = m_arr[SampleId], N = N_arr[SampleId];
	ofstream myres_file(myres_txt[SampleId]);
	for (int i = 0; i < m; i++)
	{
		for (int j = N - 1; j >= 0; j--)
			if (eliminated_rows[i]->get(j))
				myres_file << j << " ";
		myres_file << '\n';
	}
	myres_file.close(), myres_file.clear();

	auto end = chrono::high_resolution_clock::now();
	time += chrono::duration<double, std::ratio<1, 1>>(end - start).count();
}

//串行算法2：对小规模样本，一次性读入内存处理，按列序处理被消元行
// compute_time:计算时间; read_time:消元子、被消元行的读取时间; write_res_time:结果写入文件的时间
void Serial_col_major(int SampleId, double &compute_time, double &read_time, double &write_res_time)
{
	int N = N_arr[SampleId], m = m_arr[SampleId], len = len_arr[SampleId];
	malloc_(SampleId);
	read_all(SampleId, read_time);

	auto start = chrono::high_resolution_clock::now();
	for (int k = N - 1; k >= 0; k--)
	{
		//没有对应消元子，则寻找能升格的被消元行
		if (eliminators[k] == NULL)
		{
			for (int i = 0; i < m; i++)
			{
				if (eliminated_rows[i]->get(k) && !eliminated_rows[i]->upgraded)
				{
					eliminators[k] = eliminated_rows[i];
					eliminated_rows[i]->upgraded = true;
					break;
				}
			}
		}
		if (eliminators[k] != NULL)
		{
			for (int i = 0; i < m; i++)
			{
				if (eliminated_rows[i]->get(k) && !eliminated_rows[i]->upgraded)
				{
					for (int j = eliminated_rows[i]->index(k); j < len; j++)
						eliminated_rows[i]->bits[j] ^= eliminators[k]->bits[j];
				}
			}
		}
	}
	auto end = chrono::high_resolution_clock::now();
	compute_time += chrono::duration<double, std::ratio<1, 1>>(end - start).count();

	write_all_res(SampleId, write_res_time);
	free_(SampleId);
}

/************************************************************************************************************************************************************************************
 * 下面是内存算法的并行版本
 ************************************************************************************************************************************************************************************/
void Parallel_col_major(int SampleId, double &compute_time, double &read_time, double &write_res_time, int NUM_THREADS)
{
	int N = N_arr[SampleId], m = m_arr[SampleId], len = len_arr[SampleId];
	malloc_(SampleId);
	read_all(SampleId, read_time);

	auto start = chrono::high_resolution_clock::now();
	int i, j, k;
#pragma omp parallel num_threads(NUM_THREADS), private(i, j, k)
	for (k = N - 1; k >= 0; k--)
	{
#pragma omp single
		//没有对应消元子，则寻找能升格的被消元行
		if (eliminators[k] == NULL)
		{
			for (i = 0; i < m; i++)
			{
				if (eliminated_rows[i]->get(k) && !eliminated_rows[i]->upgraded)
				{
					eliminators[k] = eliminated_rows[i];
					eliminated_rows[i]->upgraded = true;
					break;
				}
			}
		}
		if (eliminators[k] != NULL)
		{
#pragma omp for
			for (i = 0; i < m; i++)
			{
				if (eliminated_rows[i]->get(k) && !eliminated_rows[i]->upgraded)
				{
					// AVX512并行异或
					for (j = eliminated_rows[i]->index(k); j + 16 <= len; j += 16)
					{
						__m512i va = _mm512_loadu_si512(&(eliminated_rows[i]->bits[j]));
						__m512i vb = _mm512_loadu_si512(&(eliminators[k]->bits[j]));
						va = _mm512_xor_si512(va, vb);
						_mm512_storeu_si512(&(eliminated_rows[i]->bits[j]), va);
					}
					for (; j < len; j++)
						eliminated_rows[i]->bits[j] ^= eliminators[k]->bits[j];
				}
			}
		}
	}
	auto end = chrono::high_resolution_clock::now();
	compute_time += chrono::duration<double, std::ratio<1, 1>>(end - start).count();

	write_all_res(SampleId, write_res_time);
	free_(SampleId);
}

/************************************************************************************************************************************************************************************
 * 主函数测试各个样本
 ************************************************************************************************************************************************************************************/
int main(int argc, char *argv[])
{
	// //测试串行算法
	// //小规模的情况一次性处理
	// for (int SampleId = 0; SampleId < 7; SampleId++)
	// {
	// 	double total_compute_time = 0, total_read_time = 0, total_write_time = 0, counter = 0;
	// 	do
	// 	{
	// 		Serial_col_major(SampleId, total_compute_time, total_read_time, total_write_time);
	// 		verify(SampleId);
	// 		counter++;
	// 	} while (counter < total_count_arr[SampleId]);
	// 	printf("Serial: Sample%d Average_Compute_Time = %.9lfs Average_Read_Time = %.9lfs Average_Write_Time = %.9lfs\n", SampleId + 1, total_compute_time / counter, total_read_time / counter, total_write_time / counter);
	// }
	// //大规模的情况分批处理
	// for (int SampleId = 7; SampleId < 11; SampleId++)
	// {
	// 	int batch_size = 1000; //每批处理的被消元行数
	// 	int delta_lp = 500;	   //每批处理的消元子数
	// 	double time_arr[7] = {0.0};
	// 	Serial_row_major(batch_size, delta_lp, SampleId, time_arr);
	// 	verify(SampleId);
	// 	printf("Serial: Sample%d Compute=%.9lfs, Preprocess_eliminator=%.9lfs, Preprocess_eliminated=%.9lfs, Decode_eliminated=%.9lfs, Decode_eliminator=%.9lfs, Write_new_eliminator=%.9lfs, Write_res=%.9lfs\n", SampleId + 1, time_arr[0], time_arr[1], time_arr[2], time_arr[3], time_arr[4], time_arr[5], time_arr[6]);
	// 	// system("pause");
	// }
	//测试并行的内存算法
	for (int SampleId = 0; SampleId < 7; SampleId++)
	{
		double total_compute_time = 0, total_read_time = 0, total_write_time = 0, counter = 0;
		int NUM_THREADS = 4;
		do
		{
			Parallel_col_major(SampleId, total_compute_time, total_read_time, total_write_time, NUM_THREADS);
			verify(SampleId);
			counter++;
		} while (counter < total_count_arr[SampleId]);
		printf("Parallel: Sample%d Average_Compute_Time = %.9lfs Average_Read_Time = %.9lfs Average_Write_Time = %.9lfs\n", SampleId + 1, total_compute_time / counter, total_read_time / counter, total_write_time / counter);
	}
	//测试并行的外存算法
	for (int SampleId = 7; SampleId < 11; SampleId++)
	{
		double total_time = 0, useless_time = 0;
		int batch_size = 1000; //每批处理的被消元行数
		int delta_lp = 500;	   //每批处理的消元子数
		Parallel_row_major(batch_size, delta_lp, SampleId, total_time, useless_time);
		verify(SampleId);
		printf("Parallel: Sample%d Total_Time = %.9lfs\n", SampleId + 1, total_time);
	}
	return 0;
}
